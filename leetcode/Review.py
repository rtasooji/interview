# Created by dark at 9/27/2019, 10:16 AM
import collections
from queue import PriorityQueue

class Solution(object):

    def twoSum(self, nums: [int], target: int) -> [int]:
        values = {}
        for i in range(len(nums)):
            if target - nums[i] in values.keys():
                return [values[target - nums[i]], i]
            else:
                values[nums[i]] = i


    def lengthOfLongestSubstring(self, s: 'str') -> 'int':
        ans = 0
        curr_sum = 0
        letter_holder = []
        for i in range(len(s) - 1):
            letter_holder.append(s[i])
            for j in range(i + 1, len(s)):
                if s[j] not in letter_holder:
                    letter_holder.append(s[j])
                else:
                    if len(letter_holder) > ans:
                        ans = len(letter_holder)
                    letter_holder = []
                    break
            letter_holder = []
        if len(letter_holder) > ans:
            ans = len(letter_holder)
        return ans

    def maxArea(self, height: [int]) -> int:
        # make two pointers a at start, b at end
        ans = 0
        p_a = 0
        p_b = len(height) - 1
        while p_b > p_a:
            # calcualte the max
            curr_vol = min(height[p_a], height[p_b]) * ((p_b - p_a) - 1)
            if curr_vol > ans:
                ans = curr_vol
            if height[p_b] > height[p_a]:
                p_a += 1
            else:
                p_b -= 1

        return ans

    def threeSum(self, nums: [int]) -> [[int]]:
        ans = []
        nums.sort()
        for i in range(len(nums) - 2):
            if i > 0 and nums[i] == nums[i - 1]:
                continue
            p_a = i + 1
            p_b = len(nums) - 1
            while p_b > p_a:
                curr_sum = nums[i] + nums[p_a] - nums[p_b]
                if curr_sum < 0:
                    p_a += 1
                elif curr_sum > 0:
                    p_b -= 1
                else:
                    ans.append([nums[i], nums[p_a], nums[p_b]])
                    while p_b > p_a and nums[p_a] == nums[p_a + 1]:
                        p_a += 1
                    while p_b > p_a and nums[p_b] == nums[p_b - 1]:
                        p_b -= 1
                    p_a += 1
                    p_b -= 1
            return ans
    class ListNode(object):
        def __init__(self, n):
            self.val = n
            self.next = None
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        dummy = curr = delay_node = ListNode(0)
        dummy.next = head
        curr.next = head
        delay_node.next = head
        curr_pos = 1
        while curr:
            curr_pos += 1
            curr = curr.next
            if curr_pos > n:
                delay_node = delay_node.next
        delay_node.next = delay_node.next.next
        return dummy.next

    def mergeTwoSortedList(self, l1: ListNode, l2: ListNode) -> ListNode:
        head = curr =ListNode(0)
        while l1 and l2:
            if l1.val > l2.val:
                curr.next = l2
                l2 = l2.next
            else:
                curr.next = l1
                l1 = l1.next
            curr = curr.next
        curr.next = l1 or l2
        return head.next

    class Wrapper(object):
        def __init__(self, node):
            self.node = node

        def __lt__(self, other: ListNode):
            return self.node.val < other.val

    def mergeKLists(self, lists: [ListNode]) -> ListNode:
        queue = PriorityQueue()
        ans = curr = ListNode("#")
        for i in lists:
            if i:
                queue.put(self.Wrapper(i))
        while not queue.empty():
            min_node = queue.get().node
            curr.next = min_node
            curr = curr.next
            min_node = min_node.next
            if min_node:
                queue.put(self.Wrapper(min_node))
        return ans.next
