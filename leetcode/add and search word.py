# Created by dark at 11/3/2019, 1:40 PM
class TrieNode(object):
    def __init__(self):
        self.links = dict()

    def put(self, char, node):
        self.links[char] = node

    def get(self, char):
        return self.links[char]


class WordDictionary(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.root = TrieNode()

    def addWord(self, word):
        """
        Adds a word into the data structure.
        :type word: str
        :rtype: None
        """
        curr_node = self.root
        for i in word:
            if i not in curr_node.links:
                node = TrieNode()
                curr_node.put(i, node)
            curr_node = curr_node.get(i)

    def search(self, word):
        """
        Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter.
        :type word: str
        :rtype: bool
        """
        if not word:
            return False
        curr_node = self.root
        ans = self.check_prefix(word, curr_node)
        return ans

    def check_prefix(self, word, curr_node):
        if len(word) == 1:
            if word == '.':
                if any(curr_node.links):
                    return True
                else:
                    return False
            else:
                if word in curr_node.links.keys():
                    return True
                else:
                    return False
        else:
            if word[0] == '.':
                for k in curr_node.links:
                    if self.check_prefix(word[1:], curr_node.get(k)):
                        return True
                return False
            else:
                if word[0] in curr_node.links:
                    if self.check_prefix(word[1:], curr_node.get(word[0])):
                        return True
                return False

word = WordDictionary()
word.addWord("add")
res = word.search("..d")
print(res)