# Created by dark at 8/26/2019, 11:02 AM
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class MySolution:

    def mergeTrees(self, t1: TreeNode, t2: TreeNode) -> TreeNode:
        if t1 is None and t2 is None:
            return None

        res = TreeNode('null')
        if t1 is None:
            res.val = t2.val
            res.left = self.mergeTrees(None, t2.left)
            res.right = self.mergeTrees(None, t2.right)
        elif t2 is None:
            res.val = t1.val
            res.left = self.mergeTrees(t1.left, None)
            res.right = self.mergeTrees(t1.right, None)
        elif t1 is not None and t2 is not None:
            res.val = t1.val + t2.val
            res.left = self.mergeTrees(t1.left, t2.left)
            res.right = self.mergeTrees(t1.right, t2.right)
        else:
            res.left = self.mergeTrees(None, None)
            res.right = self.mergeTrees(None, None)
        return res


class Solution:
    def mergeTrees(self, t1: TreeNode, t2: TreeNode) -> TreeNode:
        if t1 is None:
            return t2
        if t2 is None:
            return t1
        t = TreeNode(t1.val + t2.val)
        t.left = self.mergeTrees(t1.left, t2.left)
        t.right = self.mergeTrees(t1.right, t2.right)
        return t

class StackSolution:

    def mergeTrees(self, t1: TreeNode, t2: TreeNode) -> TreeNode:
        res = TreeNode(None)
        if t1 is None:
            return t2
        stack = []
        stack.append([t1, t2])
        while len(stack) > 0:
            nodes = stack.pop(-1)
            if nodes[0] is None or nodes[1] is None:
                continue
            nodes[0] = nodes[0].val + nodes[1].val
            if nodes[0].left is None:
                nodes[0].left = nodes[1].left
            else:
                stack.append([nodes[0].left, nodes[1].left])
            if nodes[0].right is None:
                nodes[0].right = nodes[1].right
            else:
                stack.append([nodes[0].right, nodes[1].right])
        return t1

