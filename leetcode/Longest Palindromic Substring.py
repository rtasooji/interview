# Created by dark at 9/20/2019, 5:34 PM
class Solution(object):
    def longestPalindromicSubstring(self, s: str) -> str:

        max_length = 1
        length = len(s)
        start = 0
        # for each letter
        for i in range(1, length):
            # check the letter
            low = i - 1
            high = i
            while low >= 0 and high < length and s[low] == s[high]:
                if high - low + 1 > max_length:
                    max_length = high - low + 1
                    start = low
                low -= 1
                high += 1

            low = i - 1
            high = i + 1
            while low >= 0 and high < length and s[low] == s[high]:
                if high - low + 1 > max_length:
                    max_length = high - low + 1
                    start = low
                low -= 1
                high += 1
        return s[start: start + max_length]

    def longestPalindromicSubstringDynamic(self, s: str) -> str :
        # make a matrix of true and false value
        memo = []
        max_length = 1
        start = 0
        for i in range(len(s)):
            memo.append([])
            for j in range(len(s)):
                memo[i].append(False)

        for i in range(len(s)):
            memo[i][i] = True

        for i in range(len(s) - 1):
            if s[i] == s[i + 1]:
                memo[i][i + 1] = True
                start = i
                max_length = 2

        for i in range(len(s)):
            for j in range(i + 2, len(s)):
                if s[i] == s[j] and memo[i+1][j-1]:
                    memo[i][j] = True

        return



