# Created by dark at 11/1/2019, 1:22 PM
class Solution(object):
    def canFinish(self, numCourses: int, pre: [[int]]):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: bool
        """
        course_holder = collections.defaultdict(list)
        visited = [0] * numCourses
        for i in pre:
            course_holder[i[0]].append(i[1])
        for key in course_holder.keys():
            if not self.check_pre(key, course_holder, visited):
                return False
        return True

    def check_pre(self, course, course_list, visited_list):
        """
        visited_list info:
            0: not visited
            -1: is being visited
            1: has been visited
        :type course: int
        :type course_list: collections.defaultdict(list)
        :type visited_list: [bool]
        """
        if visited_list[course] == -1:
            return False
        if visited_list[course] == 1:
            return True
        else:
            visited_list[course] = -1
            for value in course_list[course]:
                if not self.check_pre(value, course_list, visited_list):
                    return False
            visited_list[course] = 1
            return True