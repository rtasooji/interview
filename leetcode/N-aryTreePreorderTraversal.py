# Created by dark at 8/28/2019, 2:52 PM
class Node:
    def __init__(self,val, children: ):
        self.val = val
        self.children = children

class MySolution(object):
    def preorder(self, root: Node) -> [int]:
        asn = []
        ans = self.get_values(root)
        return ans

    def get_values(self, root: Node, values:[int]):
        if root is None:
            return
        if root.children = None:
            return root.val
        values.append(root.val)
        for i in range(len(root.children)):
            self.get_values(root.children[i])
        return values


class Solution(object):
    def preorder(self, root: Node) -> [int]:
        ans, q = [], root and [root] # if root is null then q will be discarded if root is not None then make a list of root
        while q:
            node = q.pop()
            ans.append(node.val)
            q.pop()
            q += [child for child in reversed(node.children) if child]
        return ans

root = Node()