# Created by dark at 8/29/2019, 1:25 PM
import collections
class Solution(object):
    def relativeSortArray(self, arr1: [int], arr2: [int]) -> [int]:
        arr1_counter = collections.Counter(arr1)
        ans = []
        for i in arr2:
            counter = arr1_counter[i]
            arr1_counter.pop(i, 0)
            for j in range(counter):
                ans.append(i)
        remaining_items = []
        for k, v in arr1_counter.items():
            for i in range(v):
                remaining_items.append(k)
        remaining_items.sort()
        for i in remaining_items:
            ans.append(i)
        return ans

sol = Solution()
A = [2,3,1,3,2,4,6,7,9,2,19]
B = [2,1,4,3,9,6]
print(sol.relativeSortArray(A, B))
