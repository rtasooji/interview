# Created by dark at 9/4/2019, 1:23 PM
import collections
class Solution:
    def commonChars(self, A: [str]) -> [str]:
        # create counter for all words
        ans = []
        counters = []
        for word in A:
            counter = collections.Counter(word)
            counters.append(counter.copy())
        # compare the first one key and value with the rest and report the common one
        first_counter = counters.pop(0)
        for key in first_counter.keys():
            is_common = True
            size = first_counter[key]
            for counter in counters:
                if key in counter.keys() and first_counter[key] == counter[key]:
                    continue
                else:
                    is_common = False
                    if key in ans:
                        ans.remove(key)
            if is_common:
                for _ in range(size):
                    ans.append(key)
        return ans

A = ["bella","label","roller"]
for i in A[0]:
    print(i.)