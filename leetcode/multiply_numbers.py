# Created by dark at 2/28/2019, 9:44 AM
def multiply(num1: str, num2: str) -> str:
    result_sum = 0
    sec_base = 0
    num1_size = len(num1)
    num2_size = len(num2)
    isNext = True
    for num1_pos in range(num1_size, 0, -1):
        base = 1
        for num2_pos in range(num2_size, 0, -1):
            if isNext:
                for i in range(sec_base):
                    base *= 10
                isNext = False
            curr_sum = int(num2[num2_pos - 1]) * int(num1[num1_pos - 1])
            curr_sum *= base
            result_sum += curr_sum
            base *= 10
        isNext = True
        sec_base += 1
    return result_sum

if __name__ == '__main__':
    print(multiply("123", "456"))