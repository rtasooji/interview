# Created by dark at 2/10/2020, 10:28 AM
class Solution(object):

    def product_except_self_mine(nums: [int]) -> [int]:
        if len(nums) == 1:
            return nums

        res = [None] * len(nums)
        left_array = [None] * len(nums)
        right_array = [None] * len(nums)
        left_array[0] = 1
        for i in range(0, len(nums) - 1):
            left_array[i+1] = left_array[i] * nums[i]

        right_array[len(nums) - 1] = 1
        for i in range(len(nums) - 1, 0, -1):
            right_array[i - 1] = right_array[i] * nums[i]
        for i in range(len(left_array)):
            res[i] = left_array[i] * right_array[i]
        return res

    def product_except_self(nums: [int]) -> [int]:
        res =  [None] * len(nums)
        res[0] = 1
        for i in range(1, len(nums)):
            res[i] = res[i-1] * nums[i-1]
        print(res)
        right = nums[len(nums) - 1]
        for i in range(len(nums) - 2, -1, -1):
            res[i] *= right
            right *= nums[i]
        return res


if __name__ == '__main__':
    A = [1, 2, 3, 4]
    B = [1]
    result = Solution.product_except_self(B)
    print(result)