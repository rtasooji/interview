# Created by dark at 8/27/2019, 11:37 AM
class MySolution(object):
    def arrayPairSum(self, nums: [int]) -> int:
        # sort array
        # python sorting ascending by defautl, to make in descending use revers=True
        nums.sort()
        # ignoring the pairing section
        # sum values at even index and sum them up
        return sum(nums[::2])