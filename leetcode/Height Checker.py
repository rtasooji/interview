# Created by dark at 8/28/2019, 12:26 PM
class Solution(object):
    def heightChecker(self, height: [int]) -> int:
        ans = 0
        sorted_h = height.copy()
        sorted_h.sort()
        for i in range(len(sorted_h)):
            if sorted_h[i] == height[i]:
                ans += 1
        return ans