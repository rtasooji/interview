# Created by dark at 2/17/2020, 3:42 PM
"""
problem:

>   There is a new alien language which uses the latin alphabet. However, the order among letters
>   are unknown to you. You receive a list of non-empty words from the dictionary, where words are sorted
>   lexicographically by the rules of this new language. Derive the order of letters in this language.

input = ["wrt", "wrf", "er", "ett", "rftt"]
output = "wertf"

Input = ["z", "x"]
output = "zx"

input = ["z", "x", "z"]
output = ""

The problem is topological sorting.
The canonical application of topological sorting is in scheduling a sequence of
jobs or tasks based on their dependencies.

Notes about graph can be found in Notes on Graph in README.md file.
"""
from collections import defaultdict

class Solution(object):
    @staticmethod
    def topological_sort_kahn(input_array: list) -> str:
        
        # make a collection of nodes
        out = []
        graph = defaultdict(set)
        for letter in input_array:
            if len(letter) == 1:
                if letter[0] in graph:
                    return ""
                graph[letter[0]].add(letter[0])
            else:
                for idx in range(len(letter) - 1):
                    graph[letter[idx]].add(letter[idx + 1])


        # find the in_degree for each node

        in_degree = {}
        for k in graph:
            in_degree[k] = 0

        for k, v in graph.items():
            for char in v:
                if char != k:
                    in_degree[char] += 1
        # add all nodes with in-degree zero to queue
        queue = []
        for k, v in in_degree.items():
            if v == 0:
                if k not in queue:
                    queue.append(k)
                else:
                    return ""
        while queue:
            curr_node = queue.pop(0)
            out.append(curr_node)
            for neigh in graph[curr_node]:
                in_degree[neigh] -= 1
                if in_degree[neigh] == 0:
                    if neigh not in queue:
                        queue.append(neigh)
                    else:
                        return ""
        return "".join(out)


    @staticmethod
    def _travers_DFS(node: chr, graph: defaultdict, stack: [], visited: dict):
        visited[node] = True
        for neigh in graph[node]:
            if not visited[neigh]:
                Solution._travers_DFS(neigh, graph, stack, visited)
        stack.append(node)


    @staticmethod
    def topological_sort_DFS(input_array: list) -> str:
        # it is like DFS approach
        # we use stack to go into each letter

        # make graph
        graph = defaultdict(set)
        for letter in input_array:
            for idx in range(len(letter) - 1):
                graph[letter[idx]].add(letter[idx + 1])

        visited = {}
        for k in graph:
            visited[k] = False
        curr_node = input_array[0][0]
        out = []
        Solution._travers_DFS(curr_node, graph, out, visited)

        return "".join([char for char in out])
                    
            