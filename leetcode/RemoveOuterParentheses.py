# Created by dark at 8/24/2019, 2:23 PM

def remove_outer_parentheses_mine(S: str) -> str:
    counter = 0
    res = ""
    for i in S:
        if i == '(' and counter == 0:
            counter += 1
            continue
        if counter > 0:
            if i == ')' and counter == 1:
                counter -= 1
                continue
            if i == '(':
                counter += 1
                res += i
            if i == ')':
                counter -= 1
                res += i
    return res

def remove_outer_parentheses(S: str) -> str:
    res = []
    counter = 0
    for c in S:
        if c == '(' and counter > 0:
            res.append(c)
        if c == ')' and counter > 1:
            res.append(c)
        counter += 1 if c == '(' else -1
    return "".join(res)

