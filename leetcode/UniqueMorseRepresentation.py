# Created by dark at 8/24/2019, 3:15 PM
def uniqueMorseRepresentations_mine(words: [str]) -> int:
    morse_array = [".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.",
                   "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."]
    morse_dict = dict()
    for i in range(97, 97 + 26):
        morse_dict[i] = morse_array[i - 97]
    morse_letters = []
    for word in words:
        morse_word = ""
        for c in word:
            morse_word += morse_dict[ord(c)]
        if morse_word not in morse_letters:
            morse_letters.append(morse_word)
    return len(morse_letters)

def uniqueMorseRepresentations(words: [str]) -> int:
    morse_array = [".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.",
                   "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."]
    seen = {"".join(morse_array[ord(c) - ord('a')] for c in word) for word in words } # this add items to empty set!
    # set vs dict, set is an empty dict and uses hash map but set is hash map without value