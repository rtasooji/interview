# Created by dark at 9/6/2019, 12:20 PM
from functools import reduce
class Solution(object):
    def numPrimeArrangements(self, n: int) -> int:
        modulo = 10 ** 9 + 7
        # crate an array of size n
        values = [[]] * n
        non_prime = 0
        prime = 0
        if n == 1:
            return 1
        # set 1 as non prime
        values[0] = [0, False]
        # first mistake forgot to add one as non_prime
        non_prime += 1
        # find prime and non prime number and update the index of array
        # This is not the fastest way to find prime number
        for i in range(2, n + 1):
            for j in range(2, i):
                if i % j == 0:
                    non_prime += 1
                    values[i - 1] = [0, False]
                    break
            else:
                prime += 1
                values[i - 1] = [0, True]
        # sec mistake I add the locator inside the loop which is dump move!
        loc = 0
        while prime > 0:
            for i in range(loc, len(values)):
                if values[i][1]:
                    values[i][0] = prime
                    prime -= 1
                    # third mistake I forgot to add 1
                    loc = i + 1
                    break
        loc = 0
        while non_prime > 0:
            for i in range(loc, len(values)):
                if not values[i][1]:
                    values[i][0] = non_prime
                    non_prime -= 1
                    loc = i + 1
                    break
        # had to check this in ide to come up with the correct syntax, though
        # if statement is not needed at the end
        ans = [x[0] for x in (val for val in values) if x[0] != 0]
        # had to check reduce to come up with multiplication process
        return reduce((lambda x, y: x * y), ans) % modulo

