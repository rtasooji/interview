# Created by dark at 8/28/2019, 7:38 PM
class Solution(object):
    def sortArrayByParity(self, A: [int]) -> [int]:
        j = 1
        for i in range(0, len(A), 2):
            print(A[i] % 2)
            if A[i] % 2:
                while A[j] % 2:
                    j += 2
                A[i], A[j] = A[j], A[i]
        return A
sol = Solution()
A = [4, 2, 5, 7]
sol.sortArrayByParity(A)