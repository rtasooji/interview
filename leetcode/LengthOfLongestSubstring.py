# Created by dark at 3/6/2019, 6:18 PM
"""
Mistakes I made:
    Didn't considered the length of input, if length less than 2 then what
    for two cased such as bbbbb, didn't check the value of counter inside the not in
    Forgot to add counter and result check in else statement
"""
import collections
def lengthOfLongestSubstring_2o (s: 'str') -> 'int':
    n = len(s)
    char_holder = set() # use set to store the characters in current window
    # then we slide the index j to the right. if it is not in the hashset we slide further.
    ans = 0
    i = 0
    j = 0
    # sliding window
    while i < n and j < n:
        if s[j] not in char_holder:
            char_holder.add(s[j])
            j += 1
            ans = max(ans, j - i)
        else:
            char_holder.remove(s[i])
            i += 1
    return ans


def lengthofLongestSubstring_o(s: 'str') -> int:
    n = len(s)
    ans = 0
    char_holder = dict()
    i = 0
    for j in range(n):
        if s[j] in char_holder.keys():
            i = max(char_holder[s[j]], i)
        ans = max(ans, j - i + 1)
        char_holder[s[j]] = j + 1
    return ans


def lengthofLongestSubstring_mine_2o(s: 'str') -> int:
    n = len(s)
    char_set = set()
    i = 0
    j = 0
    ans = 0
    while i < n and j < n:
        if s[j] not in char_set:
            char_set.add(s[j])
            j += 1
            ans = max(ans, j - i)
        else:
            char_set.remove(s[i])
            i += 1
    return ans

def lengthOfLongestSubstring_mine(s: 'str') -> 'int':
    """
    o(n^3) very poor performance
    :param self:
    :param s:
    :return:
    """
    result = 0
    char_holder = []
    for i in range(len(s)):
        char_holder.clear()
        counter = 0
        curr_char = s[i]
        char_holder.append(curr_char)
        counter += 1
        if len(s) >= 2:
            for j in range(i + 1, len(s)):
                if s[j] not in char_holder:
                    char_holder.append(s[j])
                    counter += 1
                    if counter >= result:
                        result = counter
                else:
                    if counter >= result:
                        result = counter
                    break
        else:
            result = counter
    return result

def lengthOfLongestSubstring_new( s: 'str') -> 'int':
    letter_holder = []
    counter_holder = []
    if len(s) == 1:
        return 1

    for pointer_a in range(len(s) - 1):
        letter_holder.append(s[pointer_a])
        for pointer_b in range(pointer_a + 1, len(s)):
            if s[pointer_b] not in letter_holder:
                letter_holder.append(s[pointer_b])
            else:
                counter_holder.append(len(letter_holder))
                letter_holder = []
                break
        else:
            counter_holder.append(len(letter_holder))
            letter_holder = []
    return max(counter_holder)


def review(s: str) -> int:
    # using the concept of sliding window [i, j)
    visited_chars = {}
    ans = 0
    i = 0
    for j in range(len(s)):

        if s[j] in visited_chars.keys():
            l_slide_pos = visited_chars[s[j]] + 1
            if l_slide_pos > i:
                i = l_slide_pos
        visited_chars[s[j]] = j
        curr_counter = j - i + 1
        if curr_counter > ans:
            ans = curr_counter
    return ans


    # make a dictionry the traversing value is the right open side of

"""
Example:
" " -> 1
"bbbbb" -> 1
"""
if __name__ == '__main__':
    example = 'dvdf'
    print(lengthOfLongestSubstring_new(example))