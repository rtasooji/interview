# Created by dark at 10/26/2019, 3:37 PM


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:

    def reorderList(self, head: ListNode) -> None:
        # find middle point
        p1 = head  # pointer to the middle
        p2 = head.next
        while p2 and p2.next:
            p1 = p1.next
            p2 = p2.next.next
        # reverse the half
        half_rev = self.reverseList(p1.next)
        p1.next = None
        p1 = head
        p2 = half_rev
        while p1 and p2:
            tmp1 = p1.next
            tmp2 = p2.next

            p2.next = p1.next
            p1.next = p2
            p1 = tmp1
            p2 = tmp2

    def reverseList(self, head: ListNode) -> ListNode:
        prev = None
        curr = head
        while curr:
            tmp = curr.next
            curr.next = prev
            prev = curr
            curr = tmp
        return prev

    def find_mid(self):
        global head
        p1 = head
        p2 = head
        while p2.next and p2.next.next:
            p1 = p1.next
            p2 = p2.next.next
        return p1

    def reverseList2(self) -> None:
        global head

        prev = None
        current = head
        while current:
            next = current.next
            current.next = prev
            prev = current
            current = next
        head = prev

    def reorderList2(self, head: ListNode) -> None:
        nodes = []
        node = head
        while node:
            nodes.append(node)
            node.next = node

        new_list = ListNode("#")
        curr_node = new_list

        while nodes:
            curr_node.next = nodes.pop(0)
            if nodes:
                curr_node.next.next = nodes.pop()
                if nodes:
                    curr_node = curr_node.next.next
                else:
                    curr_node.next.next.next = None
                    continue
            else:
                curr_node.next.next = None
                continue
        source = new_list.next

    def reorderList2(self, head: ListNode) -> None:
        nodes = []
        node = head

        while node:
            nodes.append(node)
            node = node.next

        new_node = ListNode('#')
        curr_node = new_node
        while nodes:
            if len(nodes) == 1:
                curr_node.next = nodes.pop(0)
                curr_node.next.next = None
                continue
            curr_node.next = nodes.pop(0)
            if nodes:
                if len(nodes) == 1:
                    curr_node.next.next = nodes.pop()
                    curr_node.next.next.next = None
                    continue
                curr_node.next.next = nodes.pop()
                curr_node = curr_node.next.next

        head = curr_node.next


    def reorderListWrong(self, head: ListNode) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        # make reverse list node from head
        node = head
        nodes = []
        while node:
            nodes.append(node)
            print(id(node))
            node = node.next
        rev = ListNode('#')
        curr_loc = rev
        while nodes:
            print(id(curr_loc))
            curr_loc.next = nodes.pop()
            curr_loc = curr_loc.next

        rev_source = rev.next

        # connect
        rev_node = rev_source
        source_node = head

        new_node = ListNode('#')
        curr_node = new_node

        visited = set()
        while id(curr_node) not in visited:
            visited.add(id(source_node))
            curr_node.next = source_node
            top_next = source_node.next

            curr_node.next.next = rev_node
            down_next = rev_node.next

            source_node = top_next
            rev_node = down_next

            curr_node = curr_node.next.next

        head = new_node.next


head = ListNode(1)
head.next = ListNode(2)
head.next.next = ListNode(3)
head.next.next.next = ListNode(4)
head.next.next.next.next = ListNode(5)
sol = Solution()
sol.reorderList(head)
while head:
    print(head.val)
    head = head.next