"""
Definition of Interval.
"""
import heapq

class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

class Solution:
    """
    @param intervals: an array of meeting time intervals
    @return: the minimum number of conference rooms required
    """
    @staticmethod
    def minMeetingRooms(intervals):
        intervals.sort(key=lambda x: x.start)
        result = []
        result.append(intervals[0].end)
        for i in range(1, len(intervals)):
            curr_val = intervals[i]
            heapq.heapify(result)
            if curr_val.start >= result[0]:
                result.pop(0)
            result.append(curr_val.start)
        return len(result)


if __name__ == '__main__':
    interval_1 = Interval(6, 15)
    interval_2 = Interval(13, 20)
    interval_3 = Interval(6, 17)
    intervals = []
    intervals.extend([interval_1, interval_2, interval_3])

    result = Solution.minMeetingRooms(intervals)
    print(result)
