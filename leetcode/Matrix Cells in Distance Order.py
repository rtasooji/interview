# Created by dark at 9/5/2019, 12:00 PM
class Solution(object):
    def allCellDistOrder(self, R: int, C: int, r0: int, c0: int) -> [[int]]:
        ans = []
        # took long enough to just figure this out
        for r in range(R):
            for c in range(C):
                ans.append([r, c])
        # creating an array of values for sorting
        man_dist = [map(lambda x: abs(r0 - x[0]) + abs(c0 - x[1]),
                                                      (x for x in (col for col in ans)))]
        # zip the value to ans to use it in sorted() function
        zipped_pairs = zip(man_dist, ans)
        # returning sorted value
        return [ans for _, ans in sorted(zipped_pairs)]