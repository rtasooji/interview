# Created by dark at 2/27/2019, 8:58 PM


class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:

    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        dummy_head = ListNode(0)
        p = l1
        q = l2
        curr = dummy_head
        carry = 0
        while p is not None or q is not None:
            x = p.val if p is not None else 0
            y = q.val if q is not None else 0
            sum_value = carry + x + y
            carry = int(sum_value / 10)
            curr.next = ListNode(sum_value % 10)
            curr = curr.next
            if p is not None:
                p = p.next
            if q is not None:
                q = q.next
        if carry > 0:
            curr.next = ListNode(carry)
        return dummy_head.next


if __name__ == '__main__':
# [2,4,3]
# [5,6,4]
    l1 = ListNode(2)
    l1_next = ListNode(4)
    l1_next.next = ListNode(3)
    l1.next = l1_next

    l2 = ListNode(5)
    l2_next = ListNode(6)
    l2_next.next = ListNode(4)
    l2.next = l2_next

    check = Solution()
    result = check.addTwoNumbers(l1, l2)
    while result is not None:
        print(result.val)
        result = result.next

