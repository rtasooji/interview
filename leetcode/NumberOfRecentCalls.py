# Created by dark at 8/27/2019, 12:43
import collections


class RecentCounter(object):
    # I didn't come up the solution, and didn't understand the problem

    def __init__(self):
        self.q = collections.deque()

    def ping(self, t: int) -> int:
        self.q.append(t)
        while self.q[0] < t - 3000:
            self.q.popleft()
        return len(self.q)