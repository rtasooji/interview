# Created by dark at 10/16/2019, 2:15 PM
class Solution:


    def exist(self, board: [[str]], word: str) -> bool:
        # make is_visited array
        is_visited = []
        for r in range(len(board)):
            is_visited.append([])
            for c in range(len(board[0])):  # potential problem
                is_visited[r].append(False)
        width = len(board)
        height = len(board[0])
        stack = [char for char in word]
        if not stack:
            return True

        for r in range(len(board)):
            for c in range(len(board[r])):
                if board[r][c] == stack[0]:
                    is_visited[r][c] = True
                    if self.check_board(stack, board, width,
                                        height, r, c, is_visited):
                        return True
                    is_visited[r][c] = False
        return False

    def check_board(self, stack: [str], board: [[str]],
                    width: int, height: int, r: int, c: int, visited: [[bool]]):
        val = stack.pop(0)
        if not stack:
            return True
        else:

            # check top
            if (c - 1) >= 0 and not visited[r][c - 1] and board[r][c - 1] == stack[0]:
                visited[r][c - 1] = True
                if self.check_board(stack, board, width, height, r, c - 1, visited):
                    return True
                visited[r][c - 1] = False

            # check down
            if (c + 1) < height and not visited[r][c + 1] and board[r][c + 1] == stack[0]:
                visited[r][c + 1] = True
                if self.check_board(stack, board, width, height, r, c + 1, visited):
                    return True
                visited[r][c + 1] = False

            # check left
            if (r - 1) >= 0 and not visited[r - 1][c] and board[r - 1][c] == stack[0]:
                visited[r - 1][c] = True
                if self.check_board(stack, board, width, height, r - 1, c, visited):
                    return True
                visited[r - 1][c] = False

            # check right
            if (r + 1) < width and not visited[r + 1][c] and board[r + 1][c] == stack[0]:
                visited[r + 1][c] = True
                if self.check_board(stack, board, width, height, r + 1, c, visited):
                    return True
                visited[r + 1][c] = False

        stack.insert(0, val)

sol= Solution()
a = [["A","B","C","E"],
     ["S","F","C","S"],
     ["A","D","E","E"]]
b = "BASAD"
sol.exist(a, b)