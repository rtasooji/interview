# Created by dark at 9/10/2019, 5:26 PM
class Solution:
    def gardenNoAdjMine(self, N: int, paths: [[int]]) -> [int]:
        # make matrix graph
        graph = []
        ans = []
        for i in range(N):
            row = []
            for j in range(N):
                row.append([0, 0])
            graph.append(row)

        for path in paths:
            graph[path[0] - 1][path[1] - 1][0] = 1
            graph[path[1] - 1][path[0] - 1][0] = 1
        if self.check_nextGarden(graph, ans, N):
            return ans
        else:
            return []

    def check_nextGarden(self, graph, ans, N) -> bool:
        # make back tracking function
        if N == 0:
            return True
        for i in range(1, 5):
            if self.check_node(i, graph, N - 1):
                # set the value in graph
                ans.append(i)
                for row in range(len(graph)):
                    graph[row][N - 1][1] = i
                if self.check_nextGarden(graph, ans, N - 1):
                    return True
                else:
                    for row in range(len(graph)):
                        graph[row][N - 1] = 0
                    ans.pop()
                # assign next value to the node and check again
        return False

    def check_node(self, flower: int, graph, N) -> bool:
        # find the connected nodes
        # This check node is wrong and does not work it does not include if later on one node has connection with the other
        # just checks values in column
        connected_nodes = []
        for i in range(len(graph)):
            if i == N:
                continue
            if graph[N][i][0] == 1:
                connected_nodes.append(i)
        # for each connected node check the value of the flower
        for node in connected_nodes:
            if graph[N][node][1] != 0 and graph[N][node][1] == flower:
                return False
        return True


    def gardenNoAdj(self, N, paths):
        """
        :type N: int
        :type paths: List[List[int]]
        :rtype: List[int]
        """
        g = collections.defaultdict(list)
        for x, y in paths:
            g[x].append(y)
            g[y].append(x)
        plantdict = {i: 0 for i in range(1, N + 1)}
        for garden in g:
            pick = set(range(1, 5))
            for each in g[garden]:
                print(each)
                if plantdict[each] != 0 and plantdict[each] in pick:
                    pick.remove(plantdict[each])
            plantdict[garden] = pick.pop()
        return [plantdict[x] if plantdict[x] != 0 else 1 for x in range(1, N+1)]
import collections
sol = Solution()
nodes = 5
A = [[1, 2], [1, 3], [1, 4], [2, 1], [2, 3], [2, 5], [3, 1], [3, 2], [3, 4], [5, 2], [5, 4], [4, 1], [4, 3], [4, 5]]
print(sol.gardenNoAdj(nodes, A))