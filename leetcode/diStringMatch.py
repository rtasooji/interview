# Created by dark at 8/27/2019, 11:02 AM
class MySolution(object):
    def diStringMatch1(self, S: str) -> [int]:
        # The run time for this problem is bad
        counter = 0
        inc_array = []
        res = []
        for _ in range(len(S) + 1):
            inc_array.append(counter)
            counter += 1
        # check letters and pop from list and add to result
        for c in S:
            if c == 'I':
                # Mistake: Added to pop which is not needed
                res.append(inc_array.pop(0))
                #res.append(inc_array.pop(0))
            else:
                #
                res.append(inc_array.pop(-1))
                #res.append(inc_array.pop(-1))
        # Mistake: Forgot to add the last item
        res.append(inc_array.pop(0))
        return res

    def diStringMatch(self, S: str) -> [int]:
        # create lower and higher values
        low, high = 0, len(S)
        curr_l = ''
        res = []
        # for all letters:
        for l in S:
            curr_l = l
            # if letter is i add lower increase
            if curr_l == 'I':
                res.append(low)
                low += 1
            # if letter is
            else:
                res.append(high)
                high -= 1
            # keep current letter for last number
        # python can not do this
        # output = curr_l == 'I'? low : high

        res.append(low if curr_l == 'I' else high)
        return res