# Created by dark at 9/5/2019, 4:43 PM
class Solution(object):
    def dietPlanPerformance(self, calories: [int], k: int, lower: int, upper: int) -> int:
        # calculate the fist k items, at every step subtract the first and add the next
        ans = 0
        cal = sum(calories[0:k])
        ans += self.cal_point(cal, lower, upper)
        for i in range(k, len(calories)):
            cal += calories[i] - calories[i - k]
            ans += self.cal_point(cal, lower, upper)
        return ans

    def cal_point(self, cal: int, low: int, high: int) -> int:
        if cal < low:
            return -1
        elif cal > high:
            return 1
        else:
            return 0
