# Created by dark at 8/27/2019, 4:10 PM
class MySolution:
    def minDeletionSize(self, A: [str]) -> int:
        # make counter
        # check all possible combination
        pointer_a = len(A[0])
        pointer_b = 1

        while pointer_b < len(A[0]):
            for i in range(pointer_a):
                for j in range(i + 1, pointer_b):
                    # remove arrays with counter
                    word_list = []
                    is_true = True
                    for word in A:
                        chars = [c for c in word]
                        chars.pop(i)
                        if pointer_b > 1:
                            chars.pop(j)
                        word_list.append(chars)
                    # for remaining array check if is non-decresing order
                    for word in word_list:
                        for index in range(len(word) - 1):
                            if word[index] > word[index + 1]:
                                is_true = False
                                break
                        if is_true:
                            return pointer_b
                        else:
                            pointer_b += 1
                            break

class Solution(object):
    def minDeletionSize(self, A: [int]) -> int:
        ans = 0
        for col in zip(*A):
            if any(col[i] > col[i+1] for i in range(len(col) - 1)):
                ans += 1
        return ans

sol = MySolution()
input = ["cba", "daf", "ghi"]
sol.minDeletionSize(input)