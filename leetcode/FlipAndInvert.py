# Created by dark at 8/25/2019, 10:24 AM
def flip_invert_mine(A: [[int]]) -> [[int]]:
    for item in A:
        # mistake1: forgot to floor the value
        mid = int(len(item) / 2)
        #flip
        for i in range(mid):
            temp = item[i]
            item[i] = item[-1-i]
            item[-1-i] = temp
        #invert
        for index in range(len(item)):
            if item[index] == 0:
                item[index] = 1
            else:
                item[index] = 0
    return A


def flip_invert(A: [[int]]) -> [[int]]:
    for item in A:
        for i in range(int((len(item) + 1) / 2)):
            # valuable notes:
            #   Python bitwise operator:
            #   ^: XOR
            #   ~: flips the bits, ~1 results to -2
            #   << binary left shift
            #   >> binary right shift
            item[i], item[~i] = item[~i] ^ 1, item[i] ^ 1
    return A

def flip_invert_user(A: [[int]]) -> [[int]]:
    result = []
    for row in A:
        # [::-1],
        result.append(list(map(lambda x: 0 if x == 1 else 1, row[::-1])))
    return result


if __name__ == "__main__":
    test_case = [[1, 1, 0], [1, 0, 1], [0, 0, 0]]
    output = flip_invert_user(test_case)
    print(output)

