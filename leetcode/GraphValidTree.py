# Created by dark at 2/12/2020, 10:18 AM
"""
Problem:
Given n nodes labeled from 0 to n-1 and a list of undirected edges
(each edge is a pair of nodes), write a function to check whether
these edges make up a valid tree

What is unidirected graph?
An unidirected graph is graph that are connected together, where all the edges are bidirectional.
A unidirected graph is tree if it has following properties:

*   There is no cycle
*   The graph is connected

Very good resource:
https://mathinsight.org/definition/undirected_graph

What is valid tree?
No shared child is allowed, parents have unique child

"""
from collections import defaultdict
import json
import unittest

class Solution(object):
    @staticmethod
    def graph_valid_tree_wrong(n: int, edges: [[int, int]]) -> bool:
        """
        My understanding:
        valid graph is a graph that each node is going to be visited only once

        Need to understand why this is wrong
        To explore check the visualization, display and look at how graph is built
        Display in steps, maybe?
        """
        # make a visited dictionary for nodes
        if len(edges) == 0:
            if n > 1:
                return False
            else:
                return True
        # check nodes indges

        visited = {}
        for i in range(n):
            visited[i] = False

        for i in range(len(edges)):
            curr_node = edges[i]
            if visited[curr_node[0]] and visited[curr_node[1]]:
                return False
            visited[curr_node[1]] = True
            visited[curr_node[0]] = True
        for v in visited.values():
            if not v:
                return False
        return True

    @staticmethod
    def _has_cycle(node: int, parent: int, node_collections: dict,
                  visited: set) -> bool:
        visited.add(node)
        for neigh in node_collections[node]:
            if neigh != parent and neigh in visited:
                return True
            if neigh not in visited and Solution._has_cycle(neigh,
                                                         node,
                                                         node_collections,
                                                         visited):
                return True
        return False


    @staticmethod
    def graph_valid_tree_DFS(node_num: int, edges: [[int, int]]) -> bool:
        # if node names are not number the hash map is needed to connect
        # names to numbers
        if len(edges) == 1:
            return True if node_num == 1 else False

        node_collection = defaultdict(list)
        for edge in edges:
            node_collection[edge[0]].append(edge[1])
            node_collection[edge[1]].append(edge[0])
        visited = set()

        if Solution._has_cycle(0, -1, node_collection, visited):
            return False
        return len(visited) == node_num


    @staticmethod
    def graph_valid_tree_BFS(n: int, edges: [[int, int]]) -> bool:
        # BFS
        node_collection = defaultdict(list)
        for i in edges:
            node_collection[i[0]].append(i[1])
            node_collection[i[1]].append(i[0])

        queue = []
        visited = [False] * n
        queue.append(edges[0][0])
        visited[queue[0]] = True
        node_counter = 0
        while queue:
            top_node = queue.pop(0)
            node_counter += 1
            for i in node_collection[top_node]:
                if i in queue:
                    return False
                if not visited[i]:
                    queue.append(i)
                    visited[i] = True
        return node_counter == n

    @staticmethod
    def _find_union(parent: list, node: int):
        if parent[node] == -1:
            # return the index of the parent node
            return node
        else:
            return Solution._find_union(parent, parent[node])

    @staticmethod
    def _union(x: int, y: int, parent: list):
        parent_x = Solution._find_union(parent, x)
        parent_y = Solution._find_union(parent, y)
        if parent_x == parent_y:
            # found cycle
            return False
        parent[parent_x] = parent_y

    @staticmethod
    def graph_valid_tree_union(n: int, edges: [[int, int]]) -> bool:
        # Union set is like making a visited array but the difference is
        # it also has a parent of a node, using disjoint data structure concept

        # making graph
        if len(edges) == 1:
            return True if n == 1 else False
        graph = defaultdict(list)
        for edge in edges:
            graph[edge[0]].append(edge[1])
        parent = [-1] * n  # initialize at -1 means every node is a subset of itself

        # for all nodes find the parent, if they are not the same union them
        for edge in edges:
            curr_node = edge[0]
            neigh_node = edge[1]
            if not Solution._union(curr_node, neigh_node, parent):
                return False
        return True

    @staticmethod
    def graph_revised_bfs(n: int, edges: [[int, int]]) -> bool:
        # make graph out of edges
        graph = defaultdict(list)
        for edge in edges:
            # 0: 1, 2, 3, 4
            # 1: 0
            # 2: 0
            graph[edge[0]].append(edge[1])
            graph[edge[1]].append(edge[0])
        visited = [False] * n
        curr_node = edges[0][0]
        visited[curr_node] = True
        queue = list()
        queue.append(curr_node)
        node_counter = 0
        while queue:
            top_node = queue.pop(0)
            node_counter += 1
            for neigh in graph[top_node]:
                if neigh in queue:
                    return False
                if not visited[neigh]:
                    queue.append(neigh)
                    visited[neigh] = True
        return node_counter == n

        # check if tree is valid
        # what is valid tree?
        #   *   No cycle in graph
        #   *   all nodes should be visited


if __name__ == '__main__':
    n = 5
    edges = [[0, 1], [0, 2], [0, 3], [1, 4]]
    Solution.graph_valid_tree_DFS(n, edges)