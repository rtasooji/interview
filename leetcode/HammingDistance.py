# Created by dark at 8/26/2019, 12:56 PM
class MySolution(object):
    def hammingDistance(self, x: int, y: int) -> int:
        x = self.convert_int_byte(x)
        y = self.convert_int_byte(y)
        counter = 0
        for i in range(len(x)):
            if x[i] != y[i]:
                counter += 1
        return counter

    def convert_int_byte(self, value: int) -> list():
        q, r = divmod(value, 2)
        res = []
        # this is bad approach just did it to learn how convert different base
        for i in range(48):
            res.append(0)
        res[0] = r
        counter = 1
        while q > 0:
            q, r = divmod(q, 2)
            res[counter] = r
            counter += 1
        return res

class Solution(object):
    def hamming_distance(self, x: int, y: int) -> int:
        """

        :param x:
        :param y:
        :return:
        """
        xor = x ^ y
        count = 0
        for _ in range(32):
            count = xor & 1
            xor == xor >> 1
        return count

sol = Solution()
print(sol.hamming_distance(1, 4))
print('{0:b}'.format(12323243))