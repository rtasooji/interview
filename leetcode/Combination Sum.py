# Created by dark at 9/25/2019, 12:32 PM
class Solution(object):
    def cumbinationSum(self, nums: [int], target) -> [[int]]:
        ans = []
        temp = []
        self.backtracking(ans, a, temp, target, start)
        return ans

    def backtracking(self, ans: [[int]], a: [int], temp: [int], remains: int,
                     start: int):
        if remains < 0:
            return
        elif remains == 0:
            ans.append([i for i in temp])
        else:
            for i in range(start, len(a)):
                temp.append(a[i])
                # remains -= a[i] This is wrong we are passing a pointer and each time this will get update in each recursion
                # we need to pass a new variable inside the function by calling it inside the function
                self.backtracking(ans, a, temp, remains - a[i], i) # this will include duplicate values
                temp.pop()

    def combinationSumMine(self, a: List[int], target: int) -> List[List[int]]:
        """
        This approach does include duplicate answers in the output
        :param a:
        :param target:
        :return:
        """
        stack = []
        ans = []
        for r in a:
            for c in a:
                remains = target - c
                while sum(stack) < remains:
                    stack.append(r)
                if sum(stack) == remains:
                    stack.append(c)
                    ans.append([i for i in stack])
                stack = []
        return ans