# Created by dark at 8/26/2019, 4:36 PM
from collections import Counter


class MySolution(object):
    def count_characters(self, words: [str], chars: str) -> int:
        count = 0
        for word in words:
            is_valid = True
            temp_chars = [c for c in chars]
            for l in word:
                if l not in temp_chars:
                    is_valid = False
                    break
                else:
                    # wrong because you are removing
                    # for i in range(len(temp_chars)):
                    #     if temp_chars[i] == l:
                    #         temp_chars.remove(temp_chars[i])
                    temp_chars.remove(l)
            if is_valid:
                count += len(word)
        return count

    def count_characters_using_dict(self, words: [str], chars: str) -> int:
        count = 0
        chars_count = Counter(chars)
        for word in words:
            is_valid = True
            word_count = Counter(word)
            for k in word_count.keys():
                if k not in chars_count.keys():
                    is_valid = False
                    break
                else:
                    if chars_count[k] < word_count[k]:
                        is_valid = False
                        break
            if is_valid:
                count += len(word)
        return count
