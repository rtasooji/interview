# Created by dark at 10/15/2019, 6:45 PM
import collections


class Solution(object):

    def minWindow(self, s: str, t: str) -> str:

        if not s or not t:
            return ""
        t_dict = collections.Counter(t)
        required = len(t_dict)
        formed = 0
        ans = (float('inf'), None, None)
        l_p, r_p = 0, 0
        char_win = collections.defaultdict(int)

        while r_p < len(s):
            curr_char = s[r_p]
            char_win[curr_char] += 1
            if curr_char in t_dict and char_win[curr_char] == t_dict[curr_char]:
                formed += 1

            while l_p <= r_p and formed == required:
                length = r_p - l_p
                if length < ans[0]:
                    ans = (length, l_p, r_p)

                curr_char = s[l_p]
                char_win[curr_char] -= 1
                if curr_char in t_dict and char_win[curr_char] < t_dict[curr_char]:
                    formed -= 1
                l_p += 1
            r_p += 1
        return "" if ans[0] == float('inf') else s[ans[1]:ans[2] + 1]


    def minWindowMine(self, s: str, t: str) -> str:
        ans = {}
        l_p = 0
        prev_l_p = 0
        r_p = 0
        win = collections.defaultdict(int)
        # init left and right pointers
        for l in t:
            win[l] = 0

        for i in range(len(s)):
            if s[i] in t:
                l_p = i
                prev_l_p = i
                r_p = i
                win[s[i]] += 1
                # one mistake
                # seven mistake
                if not self.__is_desirable(win):
                    r_p += 1
                break
        # fifth mistake didn't include case one
        if len(s) == 1:
            return s
        while r_p < len(s):
            if not self.__is_desirable(win):
                # if there is change on l_pointer move r one step
                if s[r_p] in t:
                    win[s[r_p]] += 1
                # sec mistake
                if not self.__is_desirable(win):
                    r_p += 1
            else:
                ans[r_p - l_p] = (l_p, r_p)
                win[s[l_p]] -= 1
                if l_p <= r_p:
                    l_p += 1
                    # six mistake
                    while l_p <= r_p and s[l_p] not in t:
                        l_p += 1
            # third move right when left is changed
            # forth keep the last value
            if prev_l_p < l_p and r_p < len(s) - 1:
                prev_l_p = l_p
                r_p += 1
        if not ans:
            return ""
        b, e = ans[min(ans.keys())]
        return s[b:e + 1]

    def __is_desirable(self, win: collections.defaultdict) -> bool:
        res = True
        for k in win:
            if win[k] == 0:
                return False
        return res

S = "ab"
T = "b"
sol = Solution()
print(sol.minWindow(S, T))