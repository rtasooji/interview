class TrieNode(object):
    def __init__(self):
        self.links = dict()
        self.word = None

    def contain_prefix(self, word):
        return word in self.links

    def put(self, word, node):
        self.links[word] = node

    def get(self, word):
        return self.links[word]


class Solution(object):
    def make_trie(self, words):
        trie = TrieNode()
        top_node = trie
        for word in words:
            for i in word:
                if not trie.contain_prefix(i):
                    node = TrieNode()
                    trie.put(i, node)
                else:
                    node = trie.get(i)
                trie = node
            trie.word = word
            trie = top_node
        return trie

    def __find_word(self, r, c, board, trie, visited, ans):
        if (r, c) in visited:
            return None
        else:
            if not trie.contain_prefix(board[r][c]):
                pass
            else:
                node = trie.get(board[r][c])
                if node.word:
                    ans.append(node.word)
                    node.word = None
                visited.append((r, c))
                # check up
                if 0 < r < len(board):
                    self.__find_word(r - 1, c, board, node, visited, ans)
                # check right
                if c < len(board[r]) - 1:
                    self.__find_word(r, c + 1, board, node, visited, ans)
                # check left
                if 0 < c < len(board[r]):
                    self.__find_word(r, c - 1, board, node, visited, ans)
                # check down
                if r < len(board) - 1:
                    self.__find_word(r + 1, c, board, node, visited, ans)
                visited.pop()

    def findWords(self, board, words):
        """
        :type board: List[List[str]]
        :type words: List[str]
        :rtype: List[str]
        """
        trie = self.make_trie(words)
        ans = []
        visited = []
        for r in range(len(board)):
            for c in range(len(board[r])):
                self.__find_word(r, c, board, trie, visited, ans)
        return ans


