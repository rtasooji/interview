# Created by dark at 11/2/2019, 6:27 PM


class TrieNode(object):
    __R = 26

    def __init__(self):
        self.links = [None] * TrieNode.__R
        self.__is_end = False

    def contains_char(self, char: chr) -> bool:
        return self.links[char - ord('a')] is not None

    def get(self, char: chr):
        return self.links[char - ord('a')]

    def put(self, char: chr, node):
        """
        :param char: character link
        :param node: TrieNode type
        :return:
        """
        self.links[char - ord('a')] = node

    def set_end(self):
        self.__is_end = True

    def is_end(self):
        return self.__is_end


class Trie(object):

    def __init__(self):
        self.root = TrieNode()

    def insert(self, word):
        """
        Inserts a word into the trie
        :param word: str
        :return: None
        """
        curr_node = self.root
        for i in word:
            # if character does not exist
            if not curr_node.contains_char(ord(i)):
                new_node = TrieNode()
                curr_node.put(ord(i), new_node)

            curr_node = curr_node.get(ord(i))
        # we mark the node as the end of the word, there can be more
        # but this is the end of this word
        curr_node.set_end()

    def __search_prefix(self, prefix):
        curr_node = self.root
        for i in prefix:
            if curr_node.contains_char(ord(i)):
                curr_node = curr_node.get(ord(i))
            else:
                return None
        return curr_node

    def search(self, word):
        """
        Returns if the word is in the trie
        :param word: str
        :return: bool
        """
        node = self.__search_prefix(word)
        return True if node and node.is_end() else False

    def starts_with(self, prefix):
        """
        Returns if there is any word in the trie that starts with the given
        prefix
        :param prefix: str
        :return: bool
        """
        node = self.__search_prefix(prefix)
        return True if node else False


trie_root = Trie()
test = 'cat'
test2 = 'catcatcat'
trie_root.insert(test)
trie_root.insert(test2)
print(trie_root.starts_with('ca'))
print(trie_root.search('cat'))
print(trie_root.starts_with('catcat'))




