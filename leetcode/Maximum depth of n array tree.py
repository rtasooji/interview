# Created by dark at 8/29/2019, 7:01 PM
class Node(object):
    def __init__(self, val, children):
        self.val = val
        self.children = children
class Solution(object):
    def maxDepth(self, root: Node) -> int:
        if root is None:
            return 0
        depth = 0

        for child in root.childre:
            depth = max(depth, self.maxDepth(child))
        return depth + 1

    def maxDepthMine(self, root: Node, depth: int) -> int:
        if depth is None:
            if root is None:
                return 0
            else:
                ans = []
                depth = 1
                ans.append(depth)
                for child in root.children:
                    ans.append(self.maxDepthMine(child, depth))
                return max(ans)
        else:
            curr_depth = depth + 1
            children_depth = list()
            children_depth.append(curr_depth)
            for i in root.children:
                children_depth.append(self.maxDepthMine(i, curr_depth))
            return max(children_depth)
