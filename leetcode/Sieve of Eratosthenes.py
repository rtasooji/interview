# Created by dark at 9/8/2019, 12:42 PM
class Solution(object):
    def find_prime_num(self, n: int) -> [int] :
        values = [True] * (n+1)

        # start from 2 at index 1
        # each step get the first value and remove multiplied values from values
        p = 2
        while p * p <= n:
            if values[p]:
                for i in range(p * p, n + 1, p):
                    values[i] = False
            p += 1
        ans = [i for i, v in enumerate(values) if v and i != 0 and i != 1]
        return ans
sol = Solution()
print(sol.find_prime_num(10))
