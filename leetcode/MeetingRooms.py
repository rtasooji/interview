# Created by dark at 2/11/2020, 12:11 PM


class Solution(object):

    @staticmethod
    def meeting_room(time_interval: [[int, int]]) -> bool:
        """
        Given an array of start and end time check if it possible to attend all meetings.
        :param time_interval: array of time intervals
        :return: bool
        """
        if len(time_interval) == 1:
            return True
        time_interval.sort(key=lambda x: x[0])
        for i in range(1, len(time_interval)):
            if time_interval[i][0] <= time_interval[i-1][1]:
                return False
        return True


if __name__ == '__main__':
    input_a = [[0, 30], [15, 20], [5, 10]]
    input_b = [[0, 10]]
    input_c = [[0, 10], [20, 30], [12, 19]]
    print(Solution.meeting_room(input_c))

