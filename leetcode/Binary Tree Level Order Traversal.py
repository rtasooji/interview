# Created by dark at 10/20/2019, 3:42 PM
import collections

class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def levelOrder(self, root: TreeNode) -> [[int]]:
        ans = []
        if root:
            curr_level = [root]
        else:
            return []
        while curr_level:
            curr_val = []
            length = len(curr_val)
            for i in range(length):
                top_node = curr_level.pop(0)
                if top_node.left:
                    curr_val.append(top_node.left)
                if top_node.right:
                    curr_val.append(top_node.right)
                curr_val.append(top_node.val)
            ans.append(curr_val.copy())
        return ans

    def levelOrderMine(self, root: TreeNode) -> [[int]]:
        level_dict = collections.defaultdict(list)
        if root:
            level_dict[0].append(root)
        else:
            return []
        counter = 1
        while level_dict[counter - 1]:
            for i in level_dict[counter - 1]:
                if i.left:
                    level_dict[counter].append(i.left)
                if i.right:
                    level_dict[counter].append(i.right)
            counter += 1
        ans = []
        for k in level_dict:
            curr_val = []
            for i in level_dict[k]:
                if i:
                    curr_val.append(i.val)
            ans.append(curr_val.copy())
        return ans
