# Created by dark at 9/3/2019, 4:45 PM
class Solution(object):
    def numRookCaptures(self, board:[[]]) -> int:
        # find the rook
        row, col = [(r, c) for r in range(8) for c in range(8) if board[r][c] == 'R'][0]
        return row, col
A= [[".",".",".",".",".",".",".","."],[".",".",".","p",".",".",".","."],[".",".",".","R",".",".",".","p"],[".",".",".",".",".",".",".","."],[".",".",".",".",".",".",".","."],[".",".",".","p",".",".",".","."],[".",".",".",".",".",".",".","."],[".",".",".",".",".",".",".","."]]
sol = Solution()
row, col = sol.numRookCaptures(A)
print(row, col)
