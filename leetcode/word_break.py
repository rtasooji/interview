# Created by dark at 10/24/2019, 1:13 PM
import re
import collections

class Solution:

    def wordBreakGraph(self, source: str, wordDict: [str]) -> bool:
        queue = collections.deque()
        visited = set()
        while queue:
            curr_index = queue.popleft()
            visited.add(curr_index)
            for i in range(curr_index + 1, len(source) + 1):
                if i == len(source):
                    return True
                if source[curr_index:i] in wordDict:
                    queue.appendleft(i)
                    visited.add(i)
        return False

    def wordBreakDP(self, source: str, wordDict: [str]) -> bool:
        if not source:
            print(source)
            return True
        else:
            start_word = []
            for word in wordDict:
                if source.find(word) == 0:
                    start_word.append(word)
            for word in start_word:
                new_source = source[len(word):]
                ans = self.wordBreak(new_source, wordDict)
                if ans:
                    return True
        return False

    def wordBreakMine(self, source: str, wordDict: [str]) -> bool:
        # create an array of start, end from wordDict
        s = []
        e = []
        visited = []
        for word in wordDict:
            while True:
                # this won't work
                # idx = copy_str.find(word)
                indices = [m.start() for m in re.finditer(word, source)]
                for m in indices:
                    s.append(m)
                    e.append(m + len(word) - 1)
                    visited.append(False)
                else:
                    break

        # check all values if they
        for i in range(len(s)):
            if s[i] == 0:
                for j in range(i, len(visited)):
                    visited[j] = False
                ans = self.check_range(i, s, e, visited, len(source))
                if ans:
                    return True
        return False

    def check_range(self, idx: int, start: list, end: list,
                    visited_array: list, length: int):
        if end[idx] == length - 1:
            visited_array[idx] = True
            return True
        else:
            visited_array[idx] = True
            val = end[idx]
            # check val + 1 is not visited
            indices = [i for i, x in enumerate(start) if x == val + 1]
            for n_idx in indices:
                if not visited_array[n_idx]:
                    res = self.check_range(n_idx, start, end, visited_array, length)
                    if res:
                        return res


sol = Solution()
a = "catsandog"
print(a[0:5])
