# Created by dark at 8/28/2019, 11:57 AM
class Solution(object):
    def numUniqueEmailsMine(self, emails: List[str]) -> int:
        # mistakes
        #   1. forgot to update @ index after each update
        #   2. strings in python does not have remove
        #   3. set does not have append
        #   4. didn't read the problem fully and added extra step for domain section
        #   5. Removed extra part of the string when '.' appeared

        # make set to contain uniqu items return len of set
        ans = set()
        for email in emails:
            # find the location of @

            index_at = email.find('@')
            index_dot = email.find('.', 0, index_at)
            while index_dot != -1:
                email = email[0:index_dot] + email[index_dot + 1:]
                index_at -= 1
                index_dot = email.find('.', 0, index_at)

            index_plus = email.find('+', 0, index_at)
            if index_plus != -1:
                email = email[0:index_plus] + email[index_at:]
            ans.add(email)
        return len(ans)

    def numUniqueEmail(self, emails: [str]) -> int:
        ans = set()
        for email in emails:
            local, domain = email.split('@')
            if '+' in local:
                local = local[:local.index('+')]
            ans.add(local.replace('.', '') + '@' + domain)
        return len(ans)
