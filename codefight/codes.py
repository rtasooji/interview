# Created by dark at 6/9/2018, 4:44 PM


class CodeFight(object):

    @staticmethod
    def is_increasing_mine(sequence):

        for i in range(0, len(sequence)):
            for j in range(0, len(sequence)):
                j += i + 1
                if j > len(sequence) - 1:
                    break
                else:
                    if sequence[i] >= sequence[j]:
                        return False
                    else:
                        continue
        return True

    def almost_increasing_sequence_mine(self, sequence):
        for i in range(len(sequence)):
            new_array = sequence.copy()
            new_array.pop(i)
            if self.is_increasing_mine(new_array):
                return True
            else:
                continue
        return False

    def almost_increasing_sequence_tried(self, sequence):
        counter = 0
        max_set = sequence[0]
        prev_value = -100000
        for i in range(len(sequence)):
            if i == len(sequence) - 1:
                break
            first_value = sequence[i]
            sec_value = sequence[i + 1]
            if first_value < sec_value:
                """
                check the next value 
                """
                if first_value > prev_value:
                    max_set = first_value

                prev_value = sequence[i]
                continue
            else:
                """
                check the next value
                increase the counter
                """
                counter += 1
                if sec_value < max_set:
                    if i < len(sequence) - 2:
                        counter += 1
                    else:
                        continue
                prev_value = sequence[i]
                continue
        return counter < 2

    def almost_increasing_sequence(self, sequence):
        counter = 0

        max_value = -100000
        sec_max = -100000

        for i in range(len(sequence)):
            if sequence[i] > max_value:
                sec_max = max_value
                max_value = sequence[i]
            elif sequence[i] > sec_max:
                max_value = sequence[i]
                counter += 1
            else:
                counter += 1
        return counter < 2

    def matrix_element_sum(self, matrix):
        """
        get number of rows,
        get number of elements in the row
        Each element has haunted and score properties.
        each element is haunted do not sum
        else sum
        :param matrix: sum the column if there is zero in any location do not sum values below it.
        :return:
        """
        num_row = len(matrix)
        num_col = len(matrix[0])
        rooms = list()
        total = 0
        for i in range(num_col):
            room = {"isHaunted": False,
                    "total": 0}
            rooms.append(room)

        for i in range(num_row):
            for j in range(num_col):
                if matrix[i][j] == 0:
                    rooms[j]["isHaunted"] = True
                else:
                    if not rooms[j]["isHaunted"]:
                        rooms[j]["total"] += matrix[i][j]

        for i in rooms:
            total += i["total"]
        return total

    def all_longest_strings(self, input_array):
        """
        for each string in the array get the size and store it
        from the stored value
        :param input_array:
        :return:
        """
        str_list = list()
        for i in input_array:
            size = len(i)
            string = (i, size)
            str_list.append(string)
        sorted_list = sorted(str_list, key=lambda k: k[1], reverse=True)
        result = []
        max_value = -10000
        for i in sorted_list:
            if i[1] >= max_value:
                max_value = i[1]
                result.append(i[0])
            else:
                break
        return result

    def all_longest_strings2(self, input_array):
        """
        for each string in the array get the size and store it
        from the stored value
        :param input_array:
        :return:
        """
        sorted_list = sorted(input_array, key=lambda k: len(k), reverse=True)
        result = []
        max_value = -10000
        for i in sorted_list:
            if len(i) >= max_value:
                max_value = len(i)
                result.append(i)
            else:
                break
        return result

    def common_character_count(self, s1, s2):
        counter = 0
        larger_str = ""
        shorter_str = ""
        if len(s1) > len(s2):
            larger_str = s1
            shorter_str = s2
        else:
            larger_str = s2
            shorter_str = s1
        for i in range(len(larger_str)):
            for j in range(len(shorter_str)):
                if larger_str[i] == shorter_str[j]:
                    counter +=1
                    shorter_str = shorter_str[:j] + shorter_str[j+1:]
                    break
                else:
                    continue
        return counter

    def is_lucky(self, value):
        n = str(value)
        len_n = len(n)
        len_half = len_n // 2
        set1 = n[:len_half]
        set2 = n[len_half:]
        sum_set1 = 0
        sum_set2 = 0
        for i in set1:
            sum_set1 += int(i)
        for i in set2:
            sum_set2 += int(i)
        return sum_set1 == sum_set2

    def sort_by_height(self, value):
        org_list = value
        people = list()
        indices = list()
        for i in range(len(value)):
            if value[i] != -1:
                people.append(value[i])
                indices.append(i)
        people = sorted(people)
        for i in range(len(people)):
            index = indices[i]
            org_list[index] = people[i]
        return org_list

    def sort_by_height_rank3(self, a):
        """
        Very nice!
        :param a:
        :return:
        """
        l = sorted([i for i in a if i > 0])
        for n, i in enumerate(a):
            if i == -1:
                l.insert(n, i)
        return l

    def reverseParentheses_mine(self, s):
        """
        not working but good points
        :return:
        """

        str_start, str_rest = s.split("(", 1)
        str_rev, str_end = str_rest.split(")", 1)
        str_rev = ''.join(sorted(str_rev, reverse=True)) # sorted returns list, to convert use ''.join
        return str_start + str_rev + str_end
        # # find char inside "()"
        # input_list = s
        # index = 56
        # str_list = list()
        # for i in range(len(s)):
        #     if s(i) == "(":
        #         index = i
        #     if s(i) == ")":
        #         break
        #     if i > index:
        #         str_list.append(s(i))
        # # input the reverse values
        # sorted_list = sorted(str_list, reverse=True)
        # for i in range(len(sorted_list)):
        #     input_list[i+index] = sorted_list[i]
        # #
        #return input_list

    def reverseParentheses_1(self, s):
        """
        check the string if there is () in it and report in boolean
        if there is not:
            reverse the string and return it
        else:
            split the string into three:
                string before (
                string after )
                string between ()
                return (string before '(') + reverseParentheses(self, string between '()') + string after ')'
        :param s:
        :return:
        """
        is_bracket = False
        for i in s:
            if i == '(':
                is_bracket = True
                break
            else:
                continue
        if not is_bracket:
            return s
        else:
            before, rest = s.split("(", 1)
            between, after = rest.rsplit(")", 1)
            return before + self.reverseParentheses(between)[::-1] + after

    def reverseParentheses(self, s):
        characters = list(s)
        indices = list()
        for i, c in enumerate(characters):
            if c == '(':
                indices.append(i)
            elif c == ')':
                index = indices.pop()
                characters[index: i] = characters[i:index:-1]
        return ''.join(c for c in characters if c not in '()')

    def reverseParentheses_num1(self, s):
        for i in range(len(s)):
            if s[i] == "(":
                start = i
            if s[i] == ")":
                end = i
                result = s[:start] + s[start+1:end][::-1] + s[end+1:]  # [begin:end:step]
                return self.reverseParentheses_num1(result)
        return s

    def alternatingSums(self, a):
        result = [0, 0]
        for i in range(len(a)):
            if i % 2 == 0:
                result[0] += a[i]
            else:
                result[1] += a[i]
        return result
        return [sum(a[::2]), sum(a[1::2])]

    def add_border(self, picture):
        num_row = len(picture) + 2
        num_col = len(picture[0]) + 2
        row_content = list()
        result = list()
        boarder = '*'
        words = list()
        for i in picture:
            char = list(i)
            words.append(char)  # didn't convert to character here

        for i in range(num_row):
            for j in range(num_col):
                if i == 0 or i == (num_row - 1):
                    row_content.append(boarder)
                else:
                    if j == 0 or j == (num_col - 1):
                        row_content.append(boarder)
                    else:
                        row_content.append(words[i-1][j-1])  # made mistake here

            result.append(''.join(row_content.copy()))
            row_content.clear()
        return result

    def addBorder_num1(self, picture):
        l = len(picture[0]) + 2
        return ["*" * l] + [x.center(l, "*") for x in picture] + ["*" * l]

    def are_similar(self, a, b):
        are_similar = self.are_similar_helper(a, b)  # check if they are same
        if are_similar:
            return True
        elif not self.check_set(a, b):  # check if they have same values
            return False
        else:
            #start swaping the values
            # if one swap fixed the length return true

            return self.are_similar(a, b)

    @staticmethod
    def areSimilar(a, b):
        is_checked = False
        is_similar = True
        for i in range(len(a)):
            for j in range(len(b) - i):
                if a[i] == b[j + i]:
                    break
                else:
                    for x in range(len(a) - (i + 1)):
                        print("b: {}".format(b[j]))
                        print("a: {}".format(a[i + x + 1]))
                        if a[i + x + 1] == b[j + i]:
                            is_checked = True
                            is_similar = True
                            curr_value = a[i]
                            a[i] = a[i + x + 1]
                            a[i + x + 1] = curr_value
                            break
                        else:
                            is_similar = False
                    break
        return is_similar

if __name__ == '__main__':
    code = CodeFight()
    a = [2, 1, 2, 1, 1, 1, 2]
    b = [1, 1, 2, 1, 2, 1, 2]

    value = code.areSimilar(a, b)
    print(value)




