# Created by dark at 1/23/2019, 6:34 PM
def simpleSort(arr):

    n = len(arr)

    for i in range(n):
        j = 0
        stop = n - i
        while j < stop - 1:
            if arr[j] > arr[j + 1]:
                temp_value = arr[j]
                arr[j] = arr[j + 1]
                arr[j+1] = temp_value
            j += 1
    return arr

def simpleSort_online(arr):

    n = len(arr)

    for i in range(n):
        j = 0
        stop = n - i
        while j < stop - 1:
            if arr[j] > arr[j + 1]:
                arr[j], arr[j+1] = arr[j+1],arr[j]
            j += 1
    return arr

def baseConversion(n, x):
    # convert n from base x to 10
    curr_value = n
    divide = n
    counter = 0
    result = list()

    while divide > x:
        divide = int(curr_value / x)
        remains = curr_value % x
        result.insert(0, remains)
        curr_value = divide
    # return hex(base 16)
    result.insert(0, divide)

    # Converting integer list to string list
    return int("".join(map(str, result)))

def baseConversion(n, x):
    return format(int(n, x), 'x')
    # return "{:x}".format(int(n, x))
if __name__ == '__main__':
    print("{:x}".format(baseConversion(202, 10)))
