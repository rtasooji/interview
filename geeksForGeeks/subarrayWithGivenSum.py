# Created by dark at 8/21/2019, 11:32 AM
import fileinput


def subarray_with_given_sum(array: list, input_sum: int):
    # init variables:
    items_sum = 0
    sum_win = []
    curr_index = 0
    for i in array:
        items_sum += i
        curr_item = [curr_index + 1, i]
        sum_win.append(curr_item)
        if items_sum >= input_sum:
            if items_sum == input_sum:
                print(sum_win[0][0], sum_win[-1][0])
                return
            else:
                while items_sum > input_sum:
                    first_item = sum_win.pop(0)
                    items_sum -= first_item[1]
                    if items_sum == input_sum:
                        print(sum_win[0][0], sum_win[-1][0])
                        return
        curr_index += 1
    print(-1)


# My solution
# mistakes I made:
#   Huge problem with logic with return, indexing, and time complexity.
#   Check how I read line from input.
#   I return list and then parse it again which is redundant

def subarray_with_given_sum_n2(array: list, s: int):
    result = []
    # make a window of preview value

    for i in range(len(array)):
        curr_sum = array[i]
        if curr_sum == s:
            result.append(i + 1)
            result.append(i + 1)
            return result

        for curr_index in range(i + 1, len(array)):
            if curr_sum < s:
                curr_sum += array[curr_index]
                if curr_sum == s:
                    result.append(i + 1)
                    result.append(curr_index + 1)
                    return result
            else:
                break
    return result


line_counter = 0
test_num = 1
inputs = dict()

for line in fileinput.input():
    if line_counter == 0:
        line_counter += 1
        continue
    array = []
    s = []
    if line_counter % 2 == 1:
        inputs[test_num] = [[int(x) for x in line.split()]]
        s = line
    else:

        inputs[test_num].append([int(x) for x in line.split()])
        test_num += 1

    line_counter += 1

for key, value in inputs.items():
    result = subarray_with_given_sum(value[1], value[0][1])
    if result is not None and len(result) > 0:
        print(' '.join(map(str, result)))
    else:
        print(-1)



