# July 18, 2018:

From Building RESTFUl Python Webserver:


Run the vertual environemnt from powershell using .ps1

PowerShell ISE 

Set-ExecutionPolicy add RemoteSigned

and Run 


#   June 25, 2018:

ThreadPoolExecutors creates pool of threads that will live as long as we need
them. We can just create a thread once, and then constantly feed it new jobs 
to do.

__Future objects__:

Future objects are instantiated whenever we submit a task to an executor.
Future objects are objects that will, eventually,be given a value sometime
in the future.

futureObj.result(timeout=None), the timeout parameter put a time limit on the
future object.;

futureObj.add_done_callback(fn) will callback function which will be executed
at the point of the future's completion.

futureObj.running() to check the current status of the thread

futureObj.exception(timeout=None) to retrieve any exceptions throws by future

futureObj.done() returns true or false depending on the future has completed 
or been cancelled.

__The three method for unit testing future objects__:

*   futureObj.set_notify_or_notify_cancel()  to mock a future completing or
 being cancelled.
 
 futureObj.cancel()  # cancel future before it's started
 
 futureObj.set_notify_or_notify_cancel()  # returns False
 
 futureObj.running()  # if this returns True
 
 futureObj.set_notify_or_notify_cancel()  # returns True
 
 *  futureObj.set_result(result) can set the result of a future object within 
 unit test.
 
 *  futureObj.set_exception(exception) sets the exception that a future object
 should return.
 
 * the cancel method returns true if we successfully managed to cancel the 
 future object, or false if unsuccessful.
 
 __as_completed__:
 
 If we need to store all of the future objects that we submit to pool in an 
 array of future objects, we are going to use as_completed method from the 
 concurrent.futures module and process the result of all the tasks as and 
 when they are returned.
 
 __Callbacks__:
 
 When a task might takes a lot of time, you won't sit there and wait, you go
 do other things and then you ask them to call you back when they are done.
 
 So we can use __add_done_callback__ function
 
 Very useful!
 
 
 
 


---

#   June 24, 2018:
##  Crawler and debugging:

To add the path of the venv to pychamr terminal add this to setting/tools/
terminal:

shell path: /cmd.exe /k D:/phd/Summer/venvScripts/activate.bat

__line_profiler module__:

use pip to install, then use @profile decorator on functions to profile.

in command line use kernprof -l and the python file to generate .lprof.

open the file with -m line_profiler

__memory_profiler module__:

same as line_profiler use @profile to mark the function.

In command line use python -m memory_profiler name_of_the_script.py will give
the memory usage

To plot the memory usage over a set period of time, we need to use __mprof__
tool. The mprof tool takes a series of memory usage sample at a 0.1 second
intervals and then plots this usage into a series of .dat files.

to use the graph, first locate mprof file in the venv folder

in command line use python path_to_the_mprof run path_to_the_script.py

to plot in command line use python path_to_the_mprof plot



---

#   June 23, 2018
##  Communications between threads:

Extending the set class, we create a lock object, to ensure that all interactions
can only be executed by one thread at any give time, thus ensuring thread safety.

The append() function in list in python is atomic, and, as such, thread-safe.

__atomic__:

Making the operation atomic consists in using synchronization mechanisms in 
order to make sure that the operation is seen, from any other thread,
as a single, atomic (i.e. not splittable in parts), operation.
That means that any other thread, once the operation is made atomic,
will either see the value of foo before the assignment, or after the assignment.
But never the intermediate value. 

__Use of LIFO__:

depth-first-search, depth-limited-search and for reversing the order.

__priority queue__:

the one with lower weight has the highest priority and are removed first.

__Dequeu objects__:

are double-ended queues and it is thread-safe. Belongs to the collections module.
[0]: from left, [-1] from right.

The rotate function will move all the elements to right or left based on input 
value.


---
#   June 19, 2018:
##  Asyncio:

 Asyncio event loop can:
*   Register, execute and cancel calls
*   Launch sub-process and the associated transport for communication
with an external programs
*   Delegate costly function calls to a pool of Threads

Basically, all and event does is wait for events to happen before matching
each event to a function that we have explicitly matched with the said
 type of events.

 [Link](https://docs.python.org/3/library/asyncio.html) to asyncio document

 ```Python
stop()  # stops the loop at the next suitable apportunity
is_closed()  # returns True if event-loop is closed
close()  # closes non-running event loop and clears out any pending callbacks.
        # It does not wait and cause a brutal end to the event-loop.
        # Could lead to complications with non-terminated connections to
        # resources such as databases and brokers.
task3 = loop.create_task(myCoroutine())
task3.cancel() #allows us to request the cancellation of futures or coroutines
# there is no guarantee that the cancel function will definitely cancel
# your pending task.
loop.run_until_complete(main())
 ```
 ## Tasks:
*   Responsible for the execution of coroutines within an event-loop.
*   It can run in one event loop at one time.
*   To achieve parallelism, it requires multiple event loops over multiple
threads.

```Python
pending = asyncio.Task.all_tasks()  # returns a set of tasks for a given event
# if no event loop is passed in, then it defaults to show all the tasks in
# the current event-loop
print(pending)

async def main():
await asyncio.sleep(1)  # waits for a second then run all the tasks
```

##  as_completed():
Allow us to work with the results returned from all of our executed tasks,
when they are returned.
```Python
for f in as_completed(fs):
    result = yield from f
    # use result
```

## ensure_future():
will schedule the execution of a coroutine object, wrap it in a future and
then return a task object.
It both schedules our coroutine for execution while also wrapping it in a
future, so we have to ensure that this is completed before we try to
access __result()__

##  wrap_future():
acts as a simple adapter for converting __concurrent.futures.Future__ objects
into __asyncio.Future__ objects.

```python
with ThreadPoolExecutor(max_workder=4) as executor:
    future = executor.submit(task, (4))
    myWrappedFuture = asyncio.wrap_future(future)
```
## gather()
Takes a set of coroutines or futures and then returns a future object that
aggregates the results from the inputted set
```Python
asyncio.gather(myCoroutine(1), myCoroutine(2))
```
##  wait()
blocks the program until all of the futures or coroutines passed into the
first parameter of this function have successfully completed.
```Python
async def myCoroutine(i):
    print("My Coroutine{}".format(i))

loop = asyncio.get_event_loop()
try:
    tasks= []
    for i in range(4):
        tasks.append(myCoroutine(i))
        loop.run_until_complete(asyncio.wait(tasks))
finally:
    loop.close()
```
##  Futures:
They are created with the intention that they will eventually be given a
result some time in the future.

this [link](https://docs.python.org/3/library/asyncio-task.html#asyncio.Future)
 describes three distinct changes between FUTURE objecta and asyncio futures.
*  The __result()__ and __exception()__ functions, do not take a __timeout__
 parameter and raise an exception when the future isn't done yet.
*   Callbacks registered with the __add_done_callback()__ format are
always called through the __call_soon_threadsafe()__ event loops.
*   The __future__ class not compatible with the __wait__ and
 __as_completed__ functions in the __concurrent.futures__ package.

## Coroutines:
By utilizing coroutines within our asyncio-based application, we are essentially
 enabling ourselves to write asynchronous programs with the main exception
 that they run in a single-threaded context.

 There are two ways to implement coroutines:
 *  _async def myCoroutine()_ introduced in Python 3.5
 *  __@_asyncio.coroutine__ decorator

---
# June 17, 2018:
##  Synchronization between threads:

About threading in python because of Global Interpreter Lock, threading is
still an appropriate model if you want to run multiple 
I/O-bound tasks simultaneously.

__The Dining Philosophers__:

This problem introduced by Edsger Dijkstra and it is formulated by Tony Hoare.

Introduce the concept of deadlocked state.

The deadlocked state rely on key synchronization primitives (locks) in order to function correctly.

__Race Condition__:

> A race condition or race hazard is the behavior of an electronic, software,
or other system where the output is dependent on the sequence or timing of
other uncontrollable events. Example in page 79 of the book.

By wrapping the code, we ensure that Process A would first have to acquire the
lock in order to both read and update the value, and likewise, for process B.

__Critical Sections__:

The critical section of the code is any part that modify or access a Shared
 resource. These critical sections cannot, under any circumstance, be executed
 by more than one process at any one time.

 It is possible that the file could, potentially, become corrupted and useless
 with two processes writing to it.

 To fix race condition we need to use the synchronized primitives.

 __The joint method__:

 The join method, blocks the parent thread from progressing any further until
 that thread has confirmed that it has terminated.

 __locks__:

 A lock in python is a synchronization primitive that allows us to essentially
 lock our bathroom door. It can be in either a "locked" or "unlocked" state,
 and we can only acquire a lock while it's in an "unlocked" state.

__RLocks__:

Reentrant_locks or Rlocks are synchronization primitives that works much like
our standard lock primitive, but can be acquired by a thread multiple times
if that thread already owns it.

It can come in handy when we want to have thread-safe access for a method
within a class that access other class methods.

```python
Rlock._is_owned() # Return True if lock is owned by current_thread.
                  # This method is called only if _lock doesn't have _is_owned().
```

RLocks, essentially, allow us to obtain some form of thread safety in a 
recursive manner.

__Condition__:
A synchronization primitive that awaits on a signal from another thread.

The most common scenario is producer/consumer. We have a producer that publishes
messages to a queue and notifies other threads, aka the consumers, that there
are now messages waiting to be consumed on that queue.

__Semaphores and Bounded Semaphores__:

In most situations semaphores are used to guard resources with limited capacity.
Bounded semaphores checks to make sure its current value doesn't exceed its 
initial value.

If the semaphore is released too many times, it's a sign of a bug.

These bounded semaphores could, typically, be found in web server and database
implementations to guard against resource exhaustion in the event of too many 
people trying to connect at once.

It's generally, better practice to use a bounded semaphore as opposed to a normal
semaphore.

__Events__:

Events are objects that feature an internal flag that is either true or false.
We can continuously poll this event object to check what state it is in.

__Barriers__:

Barriers are a synchronization primitives and control points that can be used 
to ensure that progress is only made by a group of threads, after the point at
which all participating threads reach the same point.

---

#   June 14, 2018:
5 states of the threads:
*   New Thread
*   Runnable
*   Running
*   Not-running
*   Dead

Different type of threads:
*   POSIX: threads that are implemented to follow the IEEE POSIX 1003.1c standards. It alos called PThreads for short.
*   Windows threads: The standard the Microsoft has chosen to implement their own low-level threads. It is more simpler and overall more elegant than the POSIX threads API.

Ways to start a thread:
*   Define as a single function
*   Inheriting from the thread Class

##  Forking:

To fork a process is to create a second exact replica of the given process. The new process gets its own address space as well as exact copy of the parent's data and the code executing within the parent process. It will receive its own unique Process IDentifier (PID).

```Python
  # os.fork does not work under windows.
import os
def child():
    print("We are in the child process with PID = %d"%os.getpid())
def parent():
    print("We are in parent process with PID = %d" % os.getpid())
    newRef = os.fork()
    if newRef == 0:
        child()
    else:
        print("We are in the parent process and our child process has PID = %d" % newRef)
parent()
```
##  Daemon threads:
Daemon threads are 'essentially' threads that have no defined endpoints. They will continue to run forever until the program quits.

Why the example stopped working:
>   Some threads do background tasks, like sending keepalive packets, or performing periodic garbage collection, or whatever. These are only useful when the __main program is running__, and it's okay to kill them off once the other, non-daemon, threads have exited.

Getting the total number of active threads:
```python
threading.active_count()
```

Getting the current thread:
```python
threading.current_thread()
```

Main thread:
```Python
threading.main_thread()
```
We are able to call the main thread function from wherever we are to retrieve the main thread object.

Enumerating all threads:
```Python
threading.enumerate()
```
To query all threads, and then enumerate them easily so that we can obtain the information we need on them.

Identifying threads:
```python
thread = threading.Thread(name=threadName, target=myThread)

threading.current_thread().getName()
```
Having different name conventions for the threads that belong to same group of events can help the process of debugging easier.

##  Orphan processes:
threads that have no alive parent process. The only way to kill them is to enumerate alive threads and then kill them.

Solution to multiprocessing:

Do our process or thread creation at the start and store them in a pool so that they can sit and wait for further instructions without us having to incur these heavy costs of creation.

##  Multithreading models:
*   One user thread to one kernel thread
*   Many user-level threads to one kernel thread
*   Many user threads to many kernel threads

In python we typically go with one user thread to one kernel thread mapping, so it will take up a non-trivial amount of resources.

__One-to-one thread mapping__:

It can be expensive due to the inherent costs of creating and managing kernel-level threads, but they provide advantages in the sense that user-level threads are not subject to the same level of blocking as threads that follow a many-to-one mapping are subject to.

__Many-to-one__:

The advantages is that we can manage user-level threads efficiently; however, should if the user-level thread is blocked, the other threads that are mapped to kernel-level will also be blocked.

__Many-to-many__:

This entitle us to a great deal of power when trying to ensure the very highest of performances when working in a multithreaded environment.

---

#   June 13, 2018
Concurrency:
>   The practice of doing multiple things at the same time, but not specifically, in parallel.

Properties of concurrent systems:
*   Multiple actors
*   Shared resources
*   Rules

Parallelism is the art of executing two or more actions simultaneously as opposed to concurrency in which you make progress on two or more things at the same time.

The switching between threads is what is called as **context switch** and involves storing all the necessary information for a thread at a specific point in time, and then restoring it as a different point further down the line.
The advantages of single-core CPUs:
*   They do not require any complex communication protocols between multiple cores
*   Single-core CPUs require less power, which makes them better suited for IoT device.

##  Clock rate:
How many clock cycles a CPU can execute every seconds.

##  Time-sharing -  the task scheduler:
To ensure that every task has a chance to run through till completion, the where and when of a task's execution, however, is non-deterministic.

The danger of accessing shared resources without any form of synchronization.

## Multi-core processors:

Each core follows the following processes:
*   __Fetch__: Fetching the instructions from the program memory. This is dictated by a program counter (PC), which identifies the location of the next step to executed.
*   __Decode__: The core converts the instruction that it has just fetched, and converts it into a series of signals that will trigger various other parts of the cpu.
*   __Execute__: This is where we run the instruction that we have just fetched and decoded, and the result of this execution are then stored in a CPU register.

Four different style of computer architecture:
*   SISD: single instruction stream, single data stream
*   SIMD: single instruction stream, multiple data stream. Best example graphic processing unit.
*   MISD: multiple instruction stream, single data stream
*   MIMD: multiple instruction stream, multiple data stream

Computer memory architecture styles:
*   UMA (Uniform memory access)
*   NUMA (Non-uniform memory access)

## UMA:
An architecture style that features a shared memory space that can be utilized in a uniform manner by any number of processing cores. It also knowns as Symmetric Shared-Memory Multiprocessors (SMP)


---
#   June 12, 2018
##  Learning concurrency in python
What is thread?
>A thread can be defined as an ordered system of instructions that can be scheduled to run as a such
by operating system. These threads, typically, live within processes, and consist of a program counter,
 a stack, and a set of registers as well as an identifier. These threads are the smallest unit of execution to which a processor can allocate time.

 When two threads start sharing memory, there is no way to guarantee the order of a thread's execution.
  These issues are, primarily, caused by race conditions.

  There are two types of threads:
  * User-level threads
  * Kernel-level threads
Multithreading:
>   a processor that can run multiple threads simultaneously, which they are able to do by utilizing a single core that is able to very quikcly
switch context between multiple threads. This switching context takes place in such a small amount of time that we could be forgiven for thinking that multiple threads are running in parallel when, in face, they are not.

Advantage of using multiple threads:
*   Multiple threads are excellent for speeding up blocking I/O bound programs.
*   They are lightweight in terms of memory footprint when compared to processes
*   Threads share resources, and thus communication between them is easier.

Disadvantages:
*   CPython threads are hamstrung by the limitations of the global interpreter lock(GIL).
*   Should be aware of race condition
*   It's computationally expensive to switch context between multiple threads

##  processes

They allow us to do pretty much everything a thread can do, but the only key advantage is that they are not bound to a singular CPU core.

Processes are capable of working on multiple things at one time much as our multithreaded single office worker.

By spawning multiple processes, we face new challenges with regard of cross-process communication and ensuring that we don't hamper performance by spending too much time on the inter-process communication (IPC).

Advantage:
*   Processes can make better use of multi-core processors.
*   They are better than multiple threads at handling CPU-intensive tasks.
*   We can sidestep the limitations of the GIL by spawning multiple processes.
*   Crashing processes will not kill our entire program.
Disadvantages:
*   No shared resources between processes, we have to implement some from of IPC to able to pass it.
*   These require more memory
##  Reactive Programming:
Reactive programming is vary similar to event-driven, but instead of revolving around events, it focuses on data. More specifically, it deals, and reacts to specific data changes.


**Example of ReactiveX framework**:
>Angular uses the observer pattern, the iterator pattern, and functional programming.
We subscribe to different streams of incoming data, and then create observers that listen to specific events being triggered. When these observers are triggered, they run the code that corresponds to what has just happened.

Libraries for GPU programming:
*   PyCUDA, OpenCL, Theano
PyCUDA advantages:
*   Complete controle of the CUDA's drivers API
*   Good documentation
Disadvantegs:
*   It utilized Nvidia specific APIs, meaning if you don't have Nvidia-based graphic card, it cannot being used.
OpenCL can be better than PyCUDA
Theano [Link](http://deeplearning.net/software/theano/)

## Understanding GIL:
The Global Interpreter Lock is essentially a mutual exclusion lock which prevents multiple threads from executing Python code in parallel.
It is a lock that can only be held by one thread at any one time, and if you wanted to a thread to execute it own code, then it would first have to acquire the lock before it could proceed to execute its own code.
The advantage that this gives us is that while it is locked nothing else can run at the same time.
Both **Jython** and **IronPython** don't feature any form of Global Interpreter Lock.

##  Jython:
AN implementation of Python that works directly with the java platform. It can be used in a complementary fashion with java as a scripting language.

The advantage of using Jython is that it is possible to do some pretty cool things with it when working in Java, such as import existing Java libraries and frameworks.

##  IronPython
It is .NET equivelant of Jython and works on top of Microsoft's .NET framework. This is beneficial for .NET developers, as they are able to use Python as a fast and expressive scripting language within their .NET applications.

---
#   June 12, 2018
##  Boolean mask
Check this out
```python
value_pub_copy = value_pub[~value_pub.index.isin(missingIndex)]
```

---
# June 9, 2018

Notes from automated software testing with Python


installing virtualenv using pip install virtualenv.

What it does, it makes a virtual python from eixsiting one.

Commande used, in the directory:

```
virtualenv venv --python="C:\\Users\\rtaso\\Anaconda3\\envs\\py36\\python.exe"
```
## Lecture 3, Section 38

Before writing a test, we need to ask our self that does the class depends on any other files, or any other system like a database or a web API or it is independent.

For any function, method in class that is not depends on anything else we are going to put it in unit package.

The structure for folders will be like this:

*   Project
    *   test
        *   unit $\t$ for any independent methods

IF writing the test is really hard then it means the system is complex, which gives you an insight for our code.


# June 7, 2018

Use lock [statement](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/statements-expressions-operators/statements) to syncronize methods.

async modifyer in C# [link](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/async)

---
# June 6, 2018

Notes from Udemy Python Beyong the basic, OOP

## section 8, lecture 47
Use of pdb for debugging
```Python
import pdb

for val in values:
    mysum = 0
    pdb.set_brace()
```
Logging messages with different logging levels. [Link](https://docs.python.org/2/library/logging.html#levels)

Program has three states:
> Debug statement, info statement and warning statement

```Python
import Logging
logging.basicConfig(level=logging.INFO)  # The default value is warning
```
Some kwargs for basicConfig:
*   filename: add log into file
*   filemode: default is append *'a'*, but can be changed to *'w'* to overwrite the file.
*   format: to include datestamp in our log:
    *   %(asctime)s %(levelname)s:%(message)s
*   datefmt: %m/%d/%Y %I:%M:%S %p  can be found in strftime function of
    datetime. [link](https://docs.python.org/3.5/library/datetime.html#strftime-strptime-behavior)

timeit library is a library for testing runtime of the methods.
Example codes:
```python
import timeit
value = timeit.timeit(stmt="mydict['c']",
                      setup='mydict = {'a': 5, 'b': 6, 'c': 6}'
                      number=1000000)
print(value)
value = timeit.timeit(stmt="mydict.get('c')",
                        setup='mydict = {'a': 5, 'b': 6, 'c': 6}'
                        number=1000000)
print(value)

def testtime(this_dict, key):
    return this_dict[key]


value = timeit.timeit("testtime(mydict, key)",
                      setup="from __main__ import testtime;"
                      "mydict={'a': 5, 'b': 6, 'c': 6};"
                      "key='c'",
                      number=1000000)
print(value)
```
.get(key) does not raise exception if the key does not exist.

We don't need to create python scripts to do tests like this.
```
python -m 'indicates modules' timeit -n 'total number' 1000000 -s 'setup' last argument is the statement
```

For performance testing, it is recommanded to not make average of the time, instead use the lowest time.

## Notes for packaging:

Use PyCharm and follow the structure for packaging for every project to be able to provide easy unit testing.
py.test module that looks for test_ methods to run the test

**TODO** Pytest vs unitest: More reading is required.

Understanding **if \__name__ == "\__main__"**:
>If the python code runs without calling it using import, like run code in console, this section is going to be executed, but if it is being imported, this part will be ignored.

---
# June 2, 2018

Notes from Udemy Python Beyond the Basic, Object Oriented Programming

\__main__ is module

## section 5, lecture 34:

```python
# Making empty class, inhereted from object
class MyClass(object):
    pass

# Not inhereted
class YourClass():
    pass

myClass = MyClass()
yourClass = YourClass()

print(type(myClass))
print(myClass.__class__)
print(type(yourClass))
print(yourclass.__class__)
```
Use of slots to define instance of attributes:
*   slots will allow us to define which attributes are possible to be added to an instance and which aren't.
*   Main purpose is to save on memory

Method Resolution Order (C3):
*   The way python goes through multiple inheritance, depth instead of breadth order.

Descriptors (get, set and delete):
*   Constructor for setting and getting object's attributes.
*   Can allow the object to have read_only attributes.

Values such as email, that might change should be stored outside of the script, cause they are contents.

One key per line to make contents.

## Section 6, lecture 38
raw_input: wait for user input.

Each exception has its own type and we need to be percise about it.

In Python, when exception happens the program continues executing after except.

sys.exit() needs to be called.

Error Handling:

When Error accures:
*   Python is aware of the context we are in. and all context we've been in, known as stack trace. Referes to stack of calls.

Error can be trapped in any call, at any place along the call stack.

How to place the program into an error condition:

*   Use **raise** and re-raising type error with more meaning full message:
```Python
raise TypeError('This message will be come out!')
```
*   Use the instance of the exception class:
```Python
except ClassName as e:
    print(e.message)  # shows the message comes with the error
    print(e.args)  # If there are other messages it will appears in tuple
```

Link to python built_in excetions, check 5.4. Exception hierarchy:
[here](https://docs.python.org/3/library/exceptions.html)
## Section 6, lecture 39:

For checking if the user's format is correct:

```Python
import re  # uses regular expressions
re.search(r'^\d\d\d\d\-\d\d\-\d\d$',usr_date)
```
Example for custom Exception class:

```Python
Class MyException(Exception):
    def __init__(self, *args):
        """
        *args: accepts any number of arguments we pass here.
        Special pattern matching mode for functions and methods.
        If we pass any number of arguments to the method, the objects will be stored in a tuple called args.
        """
        if args:
            self.message = args[0]
        else:
            self.message = ''
    def __str__(self):
        """
        This method is being called with the exception is being raised.
        """
        if self.message:
            return "message {0}".format(self.message)
        else:
            return "message"
```
## Section 7, lecture 42:
### Serialization:

*   Persistent storage, methods to store objects into disk.
*   If what we store is python object then we can say the object was made persistent.
*   Building table in relational storage is not persistent.

**Pickle:**
*   it lets us to take the whole object and store it in C2, meaning in place.
*   **pickle.dumps** and **pickle.loads** write to string rather than to the file.
*   If we instantite pickel we can store multiple object into it.
*   We can store an instance of the class with the attributes with pickls
    and load them later on.
*   If you want to pickle these things, the code for them needs to be
    available.
*   Downside: it is slow.
*   cPickle is faster.

**JSON:** (JavaScript Object Notation)
*   Popular for storing and communicating structured data.
*   Although it is JSON file but it come out as dictionary.
*   Dictionary can be converted to JSON using dump.
*   There are some problem converting dict to JSON, be careful.
```Python
json.dump(conf, fh, indent = 4, separators = (',',': '))
```

**YAML:** (YAML Ain't Markup Language)
*   Not markup language because it uses white space to separate its elements.
*   Import yaml to use it.
```
pip install pyyaml
```

```Python
import pickle

testList = [1, 2, 3, 4]
with open('datafile.pkl', 'wb') as f:
    pickle.dump(testList, f)

with open('datafile.pkl', 'rb') as f:
    currList = pickle.load(f)

import json
myDict = {"Value1": "One Value",
            "Value2": "Sec Value",
            "Value3": "Third Value"}
with open("testJson2.txt", "w") as f:
    json.dump(myDict, f, indent=4, separators=(',', ':'))

print(json.__file__)
```

---
# May 31, 2018

Notes from Udemy Python Beyond the Basic, Object Oriented Programming

 ```python
def __add__(self, other)
 ```
 a + b $\to$ a.__add(b)

Some other magic function:
```python
print(a) <==> def __repr__(self)
'abc' in var  <==> var.__contains('abc')
var == 'abc' <==> var.__eq__('abc')
var[1] <==> var.__getitem__(1)
var[1:3] <==> var.__getslice__(1, 3)
len(var) <==> var.__len__()
```

function to combine list and add the first element:

```python
def combineList(list_data1, list_data2):
    sumList = list()
    zippedFile = zip(list_data1, list_data2)
    for tup in zippedFile:
        result = tup[0] + tup[1]
        sumList.append(result)
    return result
# or
sumList = [x + y for x, y in zip(list_data1, list_data2)]
```

Making list that starts with index 1:

```python
class MyList(List):
    def __getitem__(self, index):
        if index == 0:
            raise IndexError
        if index > 0:
            index -= 1
        return List.__getitem__(self, index)

    def __setitem__(self, index, value):
        if index == 0:
            raise IndexError
        if index > 0:
            index -= 1
        return List.__setitem__(self, index, value)
```
***@property decorator:***
>Designates an instance attributes as encapsulate-able though methods.

example:

```python
class GetSet(object):
    def __init__(self, value):
        self._attrval = value  # _name makes the attr private we have property decorator to get it
        def ObjectValue():
            doc = "The ObjectValue property."
            def fget(self):
                return self._attrval
            def fset(self, value):
                self._attrval = value
            def fdel(self):
                del self._attrval
            return locals()
        ObjectValue = property(**ObjectValue())


# Another approach which is more readable:
class GetSet(object):
    def __init__(self, value):
        self._attval = value

    @property
    def objectValue(self):
        return self._attval

    @objectValue.setter
    def objectValue(self, value):
        self._attval = value

    @objectValue.deleter
    def objectValue(self):
        self._attval = None

```
Explain:
>The **@** operator takes a function-that-works-on-functions and applies it to
the next defined function

```python
@property
def the_answer():
    return 42

def the_answer():
    return 42
the_answer = property(the_answer)
```
```
__slots__ can define allowable attributes. more information needs to be gathered.
```

**PEP:** (Python Enhancment Proposal)

*   Public attributes and variables: regular_lower_case
*   All functions and methods be all lowercase with underscore seperating
*   Class: capitilized
*   Constants: all_caps
*   import module* will not import \_single\_leading\_underscore
*   \__mangled_name: not to override the attribute, attribute is mangled

**with** operation:
*  Produces the object as label that will hold the object.
*   with can be useful for any class that needs clean up after doing it's job.
*   with block is contex manager

```Python
class MyClass(object):
    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        """
        These values are set when exception is accured.
        Exception has type, value and traceback.
        """
        print('exit')

with MyClass() as cc:
    # do something
```

## Remove all adjacent duplicates in string
*   I made it very complicated playing with length and counter with lots of if statement
*   Using stack is very good approach 
*   One line code using python reduce
```python
return reduce(lambda s, c: s[:-1] if s[-1] == c else s + c, S, "#")[1:]
```