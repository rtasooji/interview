# Created by dark at 2/12/2020, 11:28 AM
import networkx as nx
import json
import matplotlib.pyplot as plt


def display_valid_tree():

    with open("../test/graph_valid_tree.txt", 'r') as f:
        lines = f.readlines()
        node_num = int(lines[0])
        edges = json.loads(lines[1])

    print(node_num)
    G = nx.Graph()
    nodes = [i for i in range(node_num)]
    G.add_nodes_from(nodes)
    G.add_edges_from(edges)
    nx.draw(G, node_size=30)
    plt.show()

if __name__ == '__main__':
    display_valid_tree()