import unittest
from leetcode.GraphValidTree import Solution
import json

class TestGraphValidTree(unittest.TestCase):

    def test_input_BFS_1(self):
        n = 5
        edges = [[0, 1], [0, 2], [0, 3], [1, 4]]
        self.assertEqual(True, Solution.graph_valid_tree_BFS(n, edges))


    def test_input_BFS_2(self):
        n = 5
        edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]]
        self.assertEqual(False, Solution.graph_valid_tree_BFS(n, edges))

    def test_input_BFS_3(self):
        with open("../test/graph_valid_tree.txt", "r") as f:
            outputs = f.readlines()
            n = int(outputs[0])
            edges = json.loads(outputs[1])
        self.assertEqual(True, Solution.graph_valid_tree_BFS(n, edges))

    def test_input_BFS_4(self):
        n = 3
        edges = [[0, 1]]
        self.assertEqual(False, Solution.graph_valid_tree_BFS(n, edges))

    def test_input_BFS_5(self):
        n = 8
        edges = [[0,1],[1,2],[3,2],[4,3],[4,5],[5,6],[6,7]]
        self.assertEqual(True, Solution.graph_valid_tree_BFS(n, edges))

    def test_input_DFS_1(self):
        n = 5
        edges = [[0, 1], [0, 2], [0, 3], [1, 4]]
        self.assertEqual(True, Solution.graph_valid_tree_DFS(n, edges))

    def test_input_DFS_2(self):
        n = 5
        edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]]
        self.assertEqual(False, Solution.graph_valid_tree_DFS(n, edges))

    def test_input_DFS_3(self):
        with open("../test/graph_valid_tree.txt", "r") as f:
            outputs = f.readlines()
            n = int(outputs[0])
            edges = json.loads(outputs[1])
        self.assertEqual(True, Solution.graph_valid_tree_DFS(n, edges))

    def test_input_DFS_4(self):
        n = 3
        edges = [[0, 1]]
        self.assertEqual(False, Solution.graph_valid_tree_DFS(n, edges))

    def test_input_DFS_5(self):
        n = 8
        edges = [[0, 1], [1, 2], [3, 2], [4, 3], [4, 5], [5, 6], [6, 7]]
        self.assertEqual(True, Solution.graph_valid_tree_DFS(n, edges))

    def test_input_union_1(self):
        n = 5
        edges = [[0, 1], [0, 2], [0, 3], [1, 4]]
        self.assertEqual(True, Solution.graph_valid_tree_union(n, edges))

    def test_input_union_2(self):
        n = 5
        edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]]
        self.assertEqual(False, Solution.graph_valid_tree_union(n, edges))

    def test_input_union_3(self):
        with open("../test/graph_valid_tree.txt", "r") as f:
            outputs = f.readlines()
            n = int(outputs[0])
            edges = json.loads(outputs[1])
        self.assertEqual(True, Solution.graph_valid_tree_union(n, edges))

    def test_input_union_4(self):
        n = 3
        edges = [[0, 1]]
        self.assertEqual(False, Solution.graph_valid_tree_union(n, edges))

    def test_input_union_5(self):
        n = 8
        edges = [[0, 1], [1, 2], [3, 2], [4, 3], [4, 5], [5, 6], [6, 7]]
        self.assertEqual(True, Solution.graph_valid_tree_union(n, edges))


if __name__ == '__main__':
    unittest.main()
