# Created by dark at 2/18/2020, 12:48 PM
import unittest
from leetcode.AlienDictionary import Solution


class TestAlienDictionary(unittest.TestCase):
    def test_topological_sort_kahn_01(self):
        test = ["wrt", "wrf", "er", "ett", "rftt"]
        self.assertEqual("werft", Solution.topological_sort_kahn(test))

    def test_topological_sort_kahn_02(self):
        test = ["z", "x"]
        self.assertEqual("zx", Solution.topological_sort_kahn(test))

    def test_topo_sort_kahn_03(self):
        test = ["z", "x", "z"]
        self.assertEqual("", Solution.topological_sort_kahn(test))

