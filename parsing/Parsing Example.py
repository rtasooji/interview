# Created by dark at 11/11/2019, 5:39 PM
import pandas as pd
import os
import re
import pickle as pck

# read file from source
def read_file(path: str) -> pd.DataFrame :
    with open(path, 'r') as f:
        lines = f.readlines()
    columns = ['date', 'time', 'Air_Temp', 'Barometric_Press', 'Dew_Point', 'Relative_Humidity', 'Wind_Dir', 'Wind_Gust', 'Wind_Speed']
    for i in range(len(lines)):
        lines[i] = re.sub(r'[\s]', ' ', lines[i])
        lines[i] = re.sub(r'\s+', ' ', lines[i])
        line = lines[i].split(" ")
        lines[i] = line
    values = [x[:-1] for x in lines[1:]]
    data_frame = pd.DataFrame(values, columns=lines[0][:-1])
    data_frame["time"] = data_frame["time"].str.split(":").apply(lambda x : int(x[0]) * 3600 + int(x[1]) * 60 + int(x[2]))
    data_frame[columns[2:]] = data_frame[columns[2:]].apply(pd.to_numeric)
    data_frame[columns[0]] = pd.to_datetime(data_frame[columns[0]], format='%Y_%m_%d')
    data_frame[columns[1]] = pd.to_timedelta(data_frame[columns[1]], unit='s')
    return data_frame


def save_file(file, path, name):
    pck.dump(obj=file, file=open(os.path.join(path, name), 'ab'))


def load_file(path):
    with open(path, 'rb') as f:
        file = pck.load(f)
    return file

file_path = "D:/learning/goLang/Learning/data/LPO_weatherdata"
file_name = "Environmental_Data_Deep_Moor_2012.txt"
output_name = "env.pck"
file = os.path.join(file_path, file_name)
if os.path.isfile(os.path.join(file_path, output_name)):
    data_frame = load_file(os.path.join(file_path, output_name))
else:
    data_frame = read_file(file)
    save_file(data_frame, file_path, output_name)

print(data_frame.head(10))
