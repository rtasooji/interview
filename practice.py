# Created by dark at 1/17/2020, 12:23 PM
class Practice(object):
    def sort_by_parity(self, A:list):
        return sorted(A, key=lambda x: x % 2)

    def lowest_common_ancestor(self, root, p, q):
        # travers and store values
        visited_p = []
        self.search_tree(root, p, visited_p)
        visited_q = []
        self.search_tree(root, q, visited_q)

        # find the common value between two list
        common_val = [x for x in visited_q if x in visited_p]

    def search_tree(self, root, p, nodes):
        if p.val == root.val:
            nodes.append(root)
            return
        else:
            nodes.append(root)
            if p.val < root.val:
                self.search_tree(root.left, p, nodes)
            if p.val > root.val:
                self.search_tree(root.right, p, nodes)


if __name__ == '__main__':
    practice = Practice()
    A = [6, 2, 4, 5]
    B = [6, 2, 4, 3]
    common_val = [x for x in A if x in B]
    print(common_val)
