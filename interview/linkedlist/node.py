# Created by dark at 4/28/2019, 6:44 PM
"""
Mistakes:
Mostly naming is not good.
The linked list hast head.
"""

class Node(object):
    def __init__(self, value, next=None):
        self.next = next
        self.value = value

class LinkedList(object):
    def __init__(self):
        self.head = None

    def print_list(self):
        curr_node = self.head
        while curr_node is not None:
            print(curr_node.value)
            curr_node = curr_node.next

    def get_size(self):
        counter = 0
        curr_node = self.head
        while curr_node is not None:
            curr_node = curr_node.next
            counter += 1
        return counter

    def get_element(self, n):
        if n <= self.get_size():
            curr_node = self.head
            value = curr_node.value
            n -= 1
            while n > 0:
                curr_node = curr_node.next
                value = curr_node.value
            return value


if __name__ == "__main__":
    first_node = Node(value=0)
    sec_node = Node(value=1)
    third_node = Node(value=2)
    list = LinkedList()
    list.head = first_node
    list.head.next = sec_node
    sec_node.next = third_node
    list.print_list()

