# Created by dark at 11/3/2019, 3:16 PM
class Node(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

class Tree(object):

    def __init__(self, val):
        self.root = Node(val)

    def search(self, value):
        curr_node = self.root
        curr_node = self.__search(curr_node, value)
        return curr_node

    def __search(self, node, value):
        if value == node.val:
            return node
        else:
            res = None
            if node.left and value < node.val:
                res = self.__search(node.left, value)
            elif node.right and value > node.val:
                res = self.__search(node.right, value)
            return res

    def insert(self, value):
        self.root = self.__insert(value, self.root)

    def __insert(self, value, node):
        if not node:
            return Node(value)
        if value < node.val:
            node.left = self.__insert(value, node.left)
        else:
            node.right = self.__insert(value, node.right)
        return node

    def find_replace(self, node, is_left=True):
        if is_left:
            if not node.right:
                return node, None
            else:
                node = self.find_replace(node.right, True)
                return node
        if not is_left:
            if not node.left:
                return node, None
            else:
                node = self.find_replace(node.left, False)
                return node

    def remove_node(self, val, rec=True):
        if rec:
            return self.__remove_node(val, self.root)
        else:
            self.__remove_node_noRec(val, self.root)

    def __remove_node_noRec(self, val, node):
        curr_node = self.root
        while curr_node:
            if curr_node.val == val:
                break
            if val < curr_node.val:
                curr_node = curr_node.left
            if val > curr_node.val:
                curr_node = curr_node.right
        if curr_node:
            if not curr_node.left and not curr_node.right:
                curr_node = None

    def __remove_node(self, val, node):
        if not node:
            return None
        if val < node.val:
            node.left = self.__remove_node(val, node.left)
        if val > node.val:
            node.right = self.__remove_node(val, node.right)
        else:
            if not node.right and not node.left:
                return None
            if node.left.val == val:
                replace_node = self.find_replace(node.left, True)
                node.val = replace_node.val
                node.left = self.__remove_node(node.val, node.left)
            if node.right.val == val:
                replace_node = self.find_replace(node.right, False)
                node.val = replace_node.val
                node.right = self.__remove_node(node.val, node.right)
        return node

root = Tree(3)
root.insert(1)
root.insert(6)
root.insert(1.5)
root.insert(2)
curr_node = root.search(2)
root.remove_node(6,rec=False)
root.remove_node(2)
pass
