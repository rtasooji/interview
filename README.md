# notes from codes:

##  sort array by parity:

*   I used insert(0, x) for even num and append.(x) for odd
*   A custom sort in python has o(NlogN) complexity

```python
A.sort(key = lambda x: x % 2)
return A

# or
return sorted(A, key=lambda x: x % 2)
```

sort vs sorted:

sort will mutate the list in-place and returns None, while sorted is a function for sorting any iterable, including list and returns a copy of the argument object, sorted.



* A quick sort approach for in place replacing values:
*   for case that needs to be included:
*   *   if A[i] % 2 == 0 and A[j] % 2 == 1: then it is correct 
*   *   if A[i] % 2 == 1 and A[j] % 2 == 0: swap item and continue
*   *   if A[i] % 2 == 0 and A[j] % 2 == 0: i is correct need to find next i to swap with j
*   *   if A[i] % 2 == 1 and A[j] % 2 == 1: j is correct need to find next j to swap with i
*   Code below show this section

```python
i, j = 0, len(A) - 1
while i < j:
    if A[i] % 2 > A[j] % 2:
        A[i], A[j] = A[j], A[i]
        
    if A[i] % 2 == 0: i += 1
    if A[j] % 2 == 1: j -= 1
```

## n repeated element in size 2n array:
*   in collections modules there is Counter class that creates a dict of keys with a value
and value with a counter of items.

```python
count = collections.Counter(A)
for k in count:
    if count[k] > 1:
        return k
```

## robot returns to origin:

*   I used collection Counter to count the numbers in hash and see if R==L and U==D
*   the first location is x = 0, y = 0 so at the end it needs to end up at the same spot so:

``` python
x, y = 0
for move in moves:
    if move == 'U': y -= 1
    elif move == 'D': y += 1
    elif move == 'L': x -= -1
    elif move == 'R': x += 1
return x == y == 0
```

## Square of a sorted array

*   I squared the array and then sorted it o(nlogn)
*   Using two pointer technique, one pointer starts for value bigger than zero
and another starts with value less than zero

``` Python
res = []
# this approach will cause problem because we will have out of range index 
###
i = 0
for value in A:
    if value < 0:
        i += 1
j = i + 1
####
# instead we subtract it
j = 0
for value in A:
    if value < 0:
        j += 1
i = j - 1
 # i is the left side starting from zero to the point, j starting from positive to the end
# keep in mind j is index so j is in correct position
while i >=0 and j < len(A):
    if A[i] ** 2 > A[j] ** 2:
        res.append(A[j] ** 2)
        j += 1
    else:
        res.append(A[i] ** 2)
        i -= 1
while i >= 0:
    res.append(A[i] ** 2)
    i -= 1
while j < len(A):
    res.append(A[j] ** 2)
    j += 1
```

## self dividing numbers:
In python there are two ways to get digits:
*   convert each letter to str iterate through that
*   use python divmod function
    *   what divmod returns?
    *   a pair of numbers a tuple, consisting of quotient 
    (a result obtained by dividing one quantity by another) q and reminder r.
        
``` Python
for d in str(n):
    if d == '0' or n % int(d) > 0:
        return False
   return True
   
# or using divmod
x = n 
while x > 0:
    x, d = divmod(x, 10)
    if d == 0 or n % d > 0:
        return False
return True
```

## Notes no Binary Tree:

*   Nodes with no children are called leaves (external nodes)
*   Sibling nodes, nodes with the same parent
*   The depth of node is the number of edges from the root to the node
*   The height of node is the number of edges from the node to the deepest leaf.
*   The height of the tree is the height of root.
*   Depth First Traversals:
    *   InOrder (Left, Root, Right)
        *   Gives nodes in non-decreasing order
    *   PreOrder (Root, Left, Right)
        *    Can be used to create a copy of the tree
        *    Also used to get prefix expression on an expression tree
    *   PostOrder (Left, Right, Root)
        *   Used to delete the tree
        *   Also can be used to get postfix expression on an expression tree
*   Breadth First Traversal

   ![Traverse](./images/Tree_transverse.png)
   
   ![bfs_dfs](./images/bfs_dfs.png)

*   Two types of tree:
    *   Full Tree
    *   Complete Tree
        *   A complete binary tree is a binary tree, which is completely filled,
            with the possible exception of the bottom level, which is filled 
            from left to right.
    
## Merge two binary trees:
    
*   I used lots of if statement to solve the problem, which was not needed
*   They used two approach one using recursively which is easy to understand 
check out that the condition will be reach to null at the end, this is the one I 
didn't considered
*   The stack approach is quit interesting!
*   My mistakes:
    *   Didn't know None prints out None, in leetcode converts to null!
    *   Instead of working on the value, consider nodes for conditional statements

## Notes on Bitwise:

*   Bitwise operator in Python:
    *   __x << y__: Returns x with the bits shifted to the left by y places
        *   same as multiplying x by 2 ** y
    *   __x >> y__: Returns x with the bits shifted to the right by y places
        *   Same as dividing x by 2 ** y
    *   __x & y__: Does a ``bitwise and``.
        *   Each bit of the output is 1 if the corresponding bit of x AND y is 1
            , otherwise it's 0
    *   __x | y__: Does a ``bitwise or``
    *   __~x__: Returns two's complement of x. the number you get by switching each
                1 for a 0 and each 0 for a 1 and add 1.
        *   This is the same as -x - 1 ( can be used in array to flip values )
    *   __x ^ y__: Does a ``bitwise exclusive or``. Each bit of the output is the same
                   as the corresponding bit in x if that bin in y is 0, and it's 
                   the complement of the bit in x if that bit in y is 1
*   Masking:
    *   The act of applying a mask to a value, which bits you want to keep,
        ans which bits to clear
        *   Bitwise ANDing in order to extract a subset of the bits in the value
        *   Bitwise ORing in order to set a subset of the bits in the value
        *   Bitwise XORing in order to toggle a subset of the bits in the value
    *   Higher bits: the first bits (left side)
    *   Lower bits: last bits (right side)

``` python
xor = x ^ y
count = 0
for _ in range(32): # 8 byte == 8 * 4 = 32
    counter += xor & 1
    xor = xor >> 1
return count
```

*   Ways to convert int to binary in python:

``` python
format(5, "b")
"{0:b}".format(5)
"{0:08b}".format(5)

#   {}: places a variable into a string
#   0: takes the variable at argument position 0
#   ":" :adds formatting options for the variable ( otherwise it would be displayed as decimal 6)
#   08: formats the number of eight digits zero-padding on the left
#   b: converts the number to its binary representation       

b = bin(5)


# convert back:
int("101", 2)
```
![Bitwise cheat sheet](./images/binworksheet1.png)

## Invert bit:

```python
import math
x = 75
digits = int(math.log2(75) + 1)
for _ in range(digits):
    x = x ^ (1 << i)
```

## reverse bit:

``` python
x = 75
r = 0 
while 
```

## Hamming Distance


## Find Words That Can Be Formed by Characters:

*   I used hint to solve the problem
    *   My main approach that didn't work caused by manipulating the immutable string
        I think replacing item with '' is not a good approach
    *   If instead of using '%s' % chars to copy the value and iterating through them
        I used [a for a in chars], my first solution would had been worked.
    *   Using Counter to count letters, if letter in chars is greater or equals to 
        letters in word then the key is valid and can be added.
        
##  Big Countries

SQL Code:

```sql
SELECT column1, column2
FROM thisTable
WHERE column1 > this AND column2 < this

```

## Swap Salary:

SQL CODE:

From leetcode:

> To dynamically change the value in the column we can use UPDATE with CASE and WHEN
flow control statements

``` sql
UPDATE salary
SET 
    sex = CASE sex
          WHEN 'm' THEN 'f'
          ELSE 'm'
          END:
/* 
Easier to read
 */
UPDATE salary
SET sex =
    CASE sex
        WHEN 'm' THEN 'f'
        WHEN 'f' THEN 'm'
    END;
```

## Reformat department table

*   I didn't know the sql format
*   select id and sum when some condition is holding return another column and set it as 
    another name
*   from the result group the result by id 
*   return the new table based on the order of the id

``` sql
SELECT id, 
       SUM(CASE WHEN month = 'Jan' THEN revenue ELSE NULL END) as Jan_revenue
FROM DepartmentTable
GROUP BY id
ORDER BY id
```

## di String Match

*   My second solution is good! Made one mistake on size, added extra for no reason

##  Array Partition 1

*   I sorted the value and summed the even numbers in the array
*   The online java approach, sort the array in o(n) because it knows the size 
    of array in advance. So it uses this information.
    Didn't spend too much time to understand the approach
    
``` java
	public int arrayPairSum(int[] nums) {
		int[] exist = new int[20001];
		for (int i = 0; i < nums.length; i++) {
			exist[nums[i] + 10000]++;
		}
		int sum = 0;
		boolean odd = true;
		for (int i = 0; i < exist.length; i++) {
			while (exist[i] > 0) {
				if (odd) {
					sum += i - 10000;
				}
				odd = !odd;
				exist[i]--;
			}
		}
		return sum;
	}
```

## Number of recent calls

*   I didn't understand the question
*   The idea is you add the previous calls to linked list and check if the previous
    ping time is in the window range of the current time - 3000 if not you pop 
    it from the list, keep doing it until the range is between 3000
    in the end we check the size of the linked list
    
## MinDeletionSize

*   I didn't understand the question
*   1026 dislike with 63 like, the question is not clear
*   *A return items inside list 
*   zip will returns the first element of each item
*   Code below is nice!

``` python
for word in zip(*A):
    if any( "condition for i" for i in range(len(word)))
```

## Unique Email Address

*   I updated @ index everything which is completely unnecessary.
*   split into local and domain, then remove everything after + and then replace '.'

## N array tree preorder Traversal

*   I made separate function to pass list and append it and return it
*   instead of using [::-1] to reverse the list, using reversed(inputList) to reverse it. It is faster approach

## N array tree postOrder traversal

*   the recursive function works the same except first get child then add 
*   Some notes for iterative way

``` python
q.append([i for i in node.children if i]) # this returns a list of items
q += [i for i in node.children if i] # this adds each item to the list
``` 

## Fibonacci numbers:

*   I did the basic one and didn't come with the concept of memoization,
    Had to look at the book to see. 
*   The site provides 6 solution:
    *   Recursion
    *   Bottom-Up approach using Memoization
    *   Top-Down approach using Memoization
    *   Iterative Top-Down Approach
    *   Matrix Exponentiation
    *   Math [Link](http://demonstrations.wolfram.com/GeneralizedFibonacciSequenceAndTheGoldenRatio/)
        *   When N is big the precision in this approach is not enough
        
## subdomain visit count

*   First I used set which is wrong because set does not come with key and value.
    Then I changed it defaultdict with value zero
*   For iteration I used find index and while loop which does not needed.
    Instead split the domain section by '.' and join each part by '.' starting 
    at the index 0
    
``` python
domain = domain.split('.')
for i in range(len(domain)):
    ans['.'.join(domain[i:])] += count
```

## Relative Sort Array

*   I made a counter, add counter to a new list, sorted the remaining. Added to the end of the answer

*   elegant solution, don't forget about custom sorting function.
    *   Save the order of B to a hashmap k, the index will be used to place identical items
    *   Sort A and take k[a] as the key, because we know the maximum array size we add x plus 1000
    
``` python
k = {b: i for i, b in enumerate(B)}
return sorted(A, key=lambda x: k.get(x, 1000 + x))
```

## Maximum Depth of N-array tree:

*    I made a list of all the depth in each branch and stored it into list and 
    then returned the max value for list
*   Creating list is not required, you can check the max of depth and the children and return that.

## Available captures for rook

*   I did typical for statements
*   find the row and col in pythonic way

``` python
# the last zero gets the first element and assigning two variables will get the tuple
r, c = [(r, c) for r in range(8) for c in range(8) if board[r][c] == 'R'][0]

# to get the column value in 2d list
col = '.'join(zip(*board)[c].replace('.', ''))
```

## Find common characters 

*   I did most common one, should I know the approach to solve things?
*   Used two array, one is the common and update it with the current array
*   second approach get the intersection between the first and the rest and update it and 
    finally get the elements of the dict

## middle of the linked list

*   My approach find total items traverse again and get the middle one
*   Make two pointers, one traverse fast and one slower, the faster goes double the amount
    when it reach the end return the slow one
    
``` python
slow = fast = head
while fast and fast.next:
    slow = slow.next
    fast = fast.next.next
return slow
``` 

## Smallest Range 1:

*   I couldn't solve the problem my approach was wrong
*   The problem seeking to return the smallest possible differences between max b and min b
    so the min of max(a) and max of min(a) can be used to answer the question
    
```python
return max(0, max(A) - min(A) - 2*k)
```

## shortest distance to a character:

*   I used map and min to find the distance, there are many redundant calculations
*   traverse in right direction and calculate the distance and then traverse in left 
    direction and compare it to right and get the min value
    
``` python
prev = float('-inf')
ans = []
for i, x in enumerate(S):
    if x == C: prev = i
    ans.append(i - prev) # 0  - (-inf) = inf
prev = float('inf')
for i in range(len(S) - 1, -1, -1):
    if S[i] == C: prev = i
    ans[i] = min(ans[i], prev-i)
return ans
```   

## Matrix cells in distance order

*   Had problem to generate the matrix
*   Had problem with custom sorting, first generate the value for sort then zip it and 
    then use sorted function to use it as index for output
    
## Finding prime number Sieve of Eratosthenses method

*   My approach: for every value check values between the range if % returns zero
    then it is not prime
*   Create an array of boolean
*   each step get p and check p * (p + 1) and set it to false
*   return list of true values

``` python
values = [True] * (n+1)
p = 2
while p * p <= n:
    if values[p]:
        for i in range(p*p, n+1, p):
            values[i] = False
    p += 1
return [i for i, v in enumerate(values) if v and i != 0 and i != 1]

```

## Compare string by frequency of the smallest character:

*    I made a Counter and get the minimum value of the counter, which is redundent.
*   I went through double loop to find the value less than inside two array
*   count the number of min character for all words and sort them in ascending order
*   find the frequency of min value in the queue and find the index in the sorted array
*   count the number on the right side of the sorted array by subtracting the length by index
*   __bisect__ in python uses bisect algorithm to insert a value inside an array that is 
    already sorted.
    
``` python
sorted_w = sorted(w.count(min(w)) for w in words)
ans = []
for q in queries:
    q_freq = q.count(min(q))
    idx = bisect.bisect(sorted_w, q_freq)
    ans.append(len(q_freq) - idx)
return ans
```

## Day of the week

*   Every leap day adds one day (Feb 29) if month is less than or equal to 2 then 
    extract that year
*   1/1/1971 is Friday
*   Calculate the distance between 1971 and current time 
*   map the reminder by 7 to the dict key and return the value.
*   use python datetime module and date function
*   return (day2 - day1).days to get the number of days between two inputs.

``` python
daysOfMonth= [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
```

## Number of equivalent domino pairs
*   First I did brute force which gave time limit exceeded
*   Then I counted the number of pairs 
    *   What I missed is from the count we can calculate number of pairs combination:
    *   V * ( V - 1 ) / 2
*   One way to check two values:
    *   put min in left and bigger on right
    *   min(d[0], d[1]) * 10 + max(d[0], d[1])
        * I converted to str and reversed string and checked that 
        
``` python

return sum(v * ( v - 1 ) / 2 for v in collections.Counter(tuple(sorted(x)) for x in A).values())

```
## print in order multi-threading
*   Didn't know at all
*   [Link](https://leetcode.com/problems/print-in-order/discuss/335939/5-Python-threading-solutions-(Barrier-Lock-Event-Semaphore-Condition)-with-explanation)
*   Different methods to make threads work in order:
    *   Barrier
    *   Lock
    *   Event
    *   Semaphore
*   Barrier example:

``` python

from threading import Barrier
class Foo(object):
    def __init__(self):
        self.first_barrier = Barrier(2)
        self.second_barrier = Barrier(2)
    
    def first(self, printFirst: 'Callable[[], None]') -> None:
        printFirst()
        self.first_barrier.wait()
    
    def second(self, printSecond) -> None:
        self.first_barrier.wait()
        printSecond()
        self.second_barrier.wait()
    
    def third(self, printThird) -> None:
        self.second_barrier.wait()
        printThird()

```

## last stone weight priority queue

*   I did o(n^2), sort the array, pop the first two and insert the new value in right place using bisect

*   Using priority queue
*   The smallest value is always at the root
* __NOTE__: If we only work with min or max value using priority heap makes sense

``` python
import heapq

pq = [-x for x in A] # by multiplying -1 the max value is min value
heapq.heapify(pq)
for i in range(len(A) - 1):
    x, y = -heapq.heappop(pq), -heapq.heappop(pq)
    heapq.heappush(pq, -abs(x -y))
return -pq[0]

```

## Flower Planting WIth No Adjacent

*   I tried back tracking but didn't work the problem was in checking point
*   An easy (relatively speaking) approach is to implement color node algorithm
*   Because each node has at most 3 edge and there are three color, for each node 
    we assign the color that is not acquired by neighbor nodes
*   What learned:
    *   assign list to dict and call every item in the list by getting it from key
    *   define graph as dict of list with nodes instead of matrix

##  Valid boomerang

*   I feel I'm quit dump!
*   There is a formula for calculating the area of the triangle based on the three point
*    Another approach is to calculate the slope but to avoid calculating fraction, 
     we use multiplication  to check the slope of two points.
     
## Partition array with sum

*   didn't solve it
*   divide the sum by three and store it
*   create an array and count the sum that is equal to the division
*   if it is three then return true

##  Sum of two number equal to K

*   Didn't solve it
*   make a hash map of the reminder
*   for zero and 30 do n*(n-1)/2
*   from one to 29 calculate key[n]*key[60 -n]

## complement of base 10 integer:
*   Understand the concept between digit representation of number of the number itself
*   How XOR operation works
    *   calculate the number of power in number
    *   make num ^ (2 ** power - 1)
    
## two sum
*   Use a dictionary

## longest non repeated substring

*   Using the abstract concept of sliding window [i, j).
*   Don't forget the j moves and if needed the i updates.
    *   This means it is possible to solve the problem in o(n), 
        if we use hashmap to get visited values
*   Add dictionary this will have letters as key and index as value for dict
*   if letter is in the dict, then we know the index of it we slide the starting
    point of our window to this point, the j is already traverse through the list


## Longest Palindromic Substring

*   at each point check to location, the pointer itself, and around the pointer
    *   while the neighbor letters are the same proceed and locate start and max length
*   Dynamic programming approach: Make a matrix and sub-problems and find the answer.


##   Dynamic Programming:

*   Steps to take for dynamic programming:
    *   Characterize the structure of an optimal solution
    *   Recursively define the value of an optimal solution
    *   Compute the value of an optimal solution in a bottom-up fashion
    *   Construct an optimal solution from computed information
*   We use _optimal substructure_ to show that we can construct an optimal 
solution to a problem from optimal solutions to sub-problems.

*   Identify the optimal sub-structure property
*   Use it to define polynomial number of sub-problems. One of them
    should be the final solution
*   There is a natural ordering of these sub-problems from smallest to
    largest such that we can obtain solution to the larger sub-problem 
    by combining solutions to smaller ones.
*   Optimal value for smallest sub-problem can be computed in a straight
    forward way. Optimal value of a larger sub-problems can be computed
    from optimal value of the smaller sub-problem in an iterative fashion.
*   We will compute the value of the optimal solution and not the optimal
    solution itself.
*   Typical in DP algorithms:
    *   First focus on computing the optimal "optimal value"
    *   Create a table on the way
    *   Use the table to backtrack and create the solution
*   Main idea of Dynamic Programming:
    *   Solve the subproblems in an order that makes sure when 
        you need an answer, it's already been computed

## Container With Most Water:

*   I don't consider this as a good question.
*   Two pointer at left and right proceed and calculate the min

##  3Sum

*   sort the array
*   Make two pointers while traversing each value in the list
    *   one after the current value (pointer a)
    *   one at the end of the sorted list (pointer b)
*   if sum(currValue, value at pointer a and value at pointer b) < 0 move value at pointer a left
*   if sum > 0 move pointer b to right
*   if sum == 0 :
    *   add three values to the answer
    *   jump duplicates values for both pointer a and b
    *   move pointer a to left
    *   move pointer b to right


##  Remove n-th node from end of the list

*   First make new list node with zero as head, this will fix the list with size 1
*   make two pointers one to go through and the other starts when value is bigger than n + 1
    *   point to the one before it
    
##  Valid Parentheses

*   Using stack to count 
*   Use dict to map and be able to use multiple characters

``` python
mapping = {")": "(", "]": "["}
stack = []
for l in s:
    if l in mapping.keys():
        top_element = stack.pop() if stack else '#'
        if mapping[l] != top_element:
            return False
    else:
        stack.append(l)
return not stack # True if len(stack) == 0 else return False
```

## Merge two linked list sorted:

*   I made the problem very complex
*   I didn't know:
    *   You need a dummy one to return the curr one goes to the null at the end
        *   The next is the pointer if you change the head you are not going to have 
            anything to return.
    *   the pointer points to the items in the l1 and l2, we keep changing the next pointer
    *   If I point to l1 and then point l1 to l1.next, the pointer to l1 is going to keep 
        the address in the ram, as far as I don't change things inside that address
        
``` python
dummy = curr = ListNode(0)
while l1 and l2:
    if l1.val < l2.val:
        curr.next = l1
        l1 = l1.next
    else:
        curr.next = l2
        l2 = le.next
    curr = curr.next
curr.next = l1 or l2
return dummy.next  
``` 

``` python
dummy = ListNode(0)
dummy.next = l
b = dummy
counter = 1
while l:
    if counter > n + 1:
        b = b.next
    l = l.next
    counter += 1
```

## Merge k sorted linked lists:

*   Make a priority queue
*   Add head to the queue
*   get the smallest value from queue
*   add the next to the queue
*   items in priority queue requries comparison function
*   in python __lt__ function works as comparision
    *   we need to create a wrapper and add this function
*   heapq and priority are the same but priority queue is thread safe

``` python
From queue import PriorityQueue
head = curr = ListNode(0)
q = PriorityQueue()
class Wrapper(object):
    def __init__(self, node):
        self.node = node
    def __lt__(self, other):
        return self.node.val < other.node.val
    
for l in lists:
    if l:
        q.put(Wrapper(l))
while not q.empty():
    node = q.get().node # weired!
    curr.next = node
    curr = curr.nect
    node = node.next
    if node:
        q.put(Wrapper(node))
return head.next
```
*   Another approach is using divide and conquer
*   We know how to merge two list, using divide and conquer and merge two list
    we can merge k lists
*   __divide and conquer approach__:

``` python
def mergeKLists(self, lists:[ListNode]) -> ListNode:
amount = len(lists) 
interval = 1
while interval < amount:
    for i in range(0, amount - interval, interval * 2):
        lists[i] = self.merge2Lists(lists[i], lists[i + interval]) # see how it works!
    interval *= 2
return lists[0] if amount > 0 else ListNode('')

def merge2Lists(self, l1: ListNode, l2: ListNode) -> ListNode:
curr = head = ListNode(0)
while l1 and l2:
    if l1 < l2:
        curr.next = l1
        l1 = l1.next
    else:
        curr.next = l2
        l2 = l2.next
    curr = curr.next
curr.next = l1 or l2
return head.next
```

## Search in Rotated Sorted Array:
*   First we need to find the index of pivot (minimum value in the list)
*   From pivot we do divide and conquer by offseting the value

``` python
# find the pivot
length = len(nums)
l = 0
h = length - 1
while l < h: ## if l <= h won't return the correct value
    mid = (l + h) // 2
    if nums[mid] > h:
        l = mid + 1
    else:
        h = mid
pivot = l
l = 0
h = length - 1
# normal binary search
while l <= h:
    mid = ( l + h ) // 2
    `    ****mid_conv = (mid + pivot) % length****`  # formula to consider offset in array of size n
    if nums[mid_conv] > target:
        h = mid - 1
    elif nums[mid_conv] < target:
        l = mid + 1
    else:
        return mid_conv
return -1
```

## Combination Sum:
*   My approach included same answer twice
*   Using backtracking to solve the problem

``` python
def backtracking( ans, a, temp, remains, start):
    if remains < 0:
        return
    elif remains == 0:
        ans.append([i for i in temp])
    else:
    # this is where backtraking do its miracle
        for i in range(start, len(a)):
            temp.append(a[i])
            # don't forget to pass a new variable to the function.
            # defining variable here will make a pointer to it and keep updating
            # this variable.
            backtracking(ans, a, temp, remains - a[i], i)
            temp.pop()
    return
```

## Rotate Matrix
*   I mirrored the image then reversed each row
*   Clockwise rotate:
    *   First reverse up to down, then swap the symmetry
*   Anticlockwise rotate:
    *   First reverse left to right, then swap symmetry
    
## Group Anagrams:
*   A dictionary key needs to be immutable 
    *   either string or tuple work as a key
*   Create an array of counting string
*   or sort each string and add them as a key to dict
    * sorted() returns list in case adding as key we need to 
        convert it to string or tuple
*   Add to key 
*   return values

``` python
import collections

def groupAnagrams(strs: []) -> [[str]]:
    ans = collections.defaultdict(list)
    for s in strs:
        chars = [0] * 26
        for c in s:
            chars[ord(c) - ord('a')] += 1
        ans[tuple(chars)].append(s)
    return ans.values()
```

## Maximum Subarray:
* the o(n ^ 2) is easy to come

*   Kadane’s Algorithm:
*   The Dynamic approach is kind of tricky to come up with
    *   We know the problem looking for continous sum of array
    *   we compare the sum of preview one to the current one
    *   If sum is less than or equal to current value set the new starting point for the answer

``` python
curr_sum = best_sum = nums[0]
for i in range(1, len(nums)):
    curr_sum = max(nums[i], curr_sum + nums[i])
    if curr_sum > best_sum:
        best_sum = curr_sum
return best_sum
```

## Maximum Product Subarray:
*   The approach is like the previews problem but we are going to keep
    two values in each iteration, the max value and min value
    *   We swap the two when we reach the negative value
    *   We update min and max at each index
    
``` python
def maxProduct(nums: []):
    ans = nums[0]
    min_val = max_val = ans
    for i in range(1, len(nums)):
        if nums[i] < 0:
            max_val, min_val = min_val, max_val
        max_val = max(nums[i], nums[i] * max_val)
        min_val = min(nums[i], nums[i] * min_val)
        ans = max(ans, max_val)
    return ans
```

## Spiral Matrix
*   Came with the same solution but didn't come with breaking loop concept
*   Good example of using yield inside python
    *   Check the code file
    *   The question is more about writing a good style code
    
## Jump Game
*   Good question that uses the concept of dynamic programming
    *   The problem can be solved o(2^n) using back tracking
    *   By observing back tracking we use top down approach and memoization
        to store values that already traversed
    *   Looking at top down we convert the solution to bottom up by 
        starting from right to left instead of left to right
    *   By observing bottom up approach, we can come to greedy approach
        * starting from last index as good and go back and if the final
            index that considered as good is the first index return True
            else return False
## Merge Intervals
*   I solved it but in a weired way, I sorted based on the second value
    and popped two and lots of other things, works but better to sort 
    based on first value and just look at the second value and merge based
    on that
##  Insert Intervals
*   The value is already sorted, look at start value at each index 
    if it is bigger than interval, add it to the interval to the list 
    and all value after that an return. if end value at each index is
    less than start value of interval, insert that index to list.
    finally, merge and update the interval based on the current index
     interval, by comparing the value .
*   faster approach is to find the correct position without traversing 
    from start and using binary search tree to find the start position
    
## Unique Paths:
*   The problem is DP problem
*   I solved it with back tracking
*   looking at the problem, the base case is that the first row and
    first column value starts with one and the value on other cells
    are memo[row][col - 1] + memo[row - 1][col]
*   Time complexity o(m*n)

## Climing staris:
*   Solved it with recursive approach
    *   the time complexity is o(2 ^ n)
*   memoization approach 
    *   store value in memo at each step if the value is not zero return the 
        value
    *   the top value stored at index zero
    
``` python
def climbStairs(n) -> int:
    memo =  [0] * (n+1)
    curr_step = 0
    max_step = 10
    return count_step(0, memo, 10)

def count_step(curr_step, memo, final_step):
    if curr_step > final_step:
        return 0
    if curr_step == final_step:
        return 1
    if memo[curr_step] != 0:
        return memo[curr_step]
    else:
        memo[curr_step] = count_step(curr_step + 1, memo, final_step) + \
                          count_step(curr_step + 2, memo, final_step)
        return memo[curr_step]
```

*   Dynamic Programming:
    *   the problem can be broken into sub-problems and it contains the optimal
        substructure property.
    *   One can reach i step in one of the two ways:
        *   Taking single step from i - 1
        *   taking a step of 2 from i - 2
        
``` python
if n == 1: return 1
dp = [0] * n
dp[0] = 1
dp[1] = 2
for i in range(2, n):
    dp[i] = dp[i -1] + dp[i-2]
return dp[n-1]
```

## set matrix zeros:
*   I did first approach, find r and c and set them to zero after ward
*   second approach:
    *   find zero in row i, and col j, change values in i and j
        to MODIFIED by changing the value to something like '-inf'.
    *   Iterate over modified matrix and change '-inf' to 0
*   Efficient solution:
    *   Every time we find zero we set the top row and col values to zero
    *   From the first row and col we iterate the matrix and set the remaining 
        to zero
        
## minimum window substring:
*   Wasted the whole day
    *   First thought about dict 
    *   Felt the approach required sliding window
    *   After looking at solution was not able to code the approach
    *   The problem was hard to implement
*   How implement the sliding window
    *   make a counter dict from t
    *   make a required az length of counter
    *   make a formed as zero
    *   init r_p and l_p at 0, with ans as tuple which keeps updated when
        formed is equal to required
    *   while pointer right is less than length of string
        *   put value in a dictionary
        *   if value is in t_dict and has equal values then increase formed
        *   while formed is equal to counter and left pointer is less or equal to
            right pointer
                *   check the value and reduce it from the char_window
                *   if char_window is less than t_dict reduce formed
    *   increase right pointer
    
## Word Search
*   This is an implementation of BFS
    *   Look at neighbors go to next step 

## decode ways
*   I spent hours to come up with a solution to find all possible
    combination
*   The correct approach is to solve the problem with db
    *   The order to understand db:
        *   Consider find all path to target
        *   then climbing stairs problem
        *   and then this
    *   the subproblem will lead to the answer to the problem
    *   We have many different ways to reach to something
        *   We don't want to repeat the same calculated things twice
        *   we store what we already calculated and we hope it can be
            used in future process. This is memoization
        ```python
        memo[i] = some_function(i + 1) + some_function(i+2)
        ```
        *   Tabulation is other way, we see the problem for small 
            case. we use the answer from that to get the final answer
        ```python
        dp[i] = dp[i -2] - dp[i-1]
        ```
*   In case of decoding ways, we see what are conditions that need
    to be considered while we exploring all possible ways.
    *   here single digit should be less than equal to 9 and 
        greater than zero
    *   double digit should be greater and equal than 10 and less 
        then 27

##  Notes on data structure:
*   [Good link](https://medium.com/omarelgabrys-blog/data-structures-a-quick-comparison-6689d725b3b0) for quick comparision.
![comparison](./images/datastructure.png)

## Validate BST:
*   The binary search tree is valid if all nodes on the 
    left side of the root is less than and all nodes on right
    side are greater than the root value
*   An inorder traverse can check if all values are sorted in 
    ascending order or not
    *   travers in order then check the result
*   Better approach get the least minimum value in the tree
    *   compare to the root, then go to the right
    *   we only need to check the node on the left to the root
    *   if root.val is less that the preview root (root on left)
        then exit else update preview value and continue
##  Same tree:
*   check left and right recursively
    *   mine is not very well written
    *   better to check if two nodes does not have the same value
        rather than checking if they have same value do next
## Binary tree Level Order Traversal:
*   I used dictionary, the key as level and list of nodes 
    in that level as values.
    *   we start at top, insert the value, and at each level
        we check values in previews level has child or not
    *   traverse at each level and get the value
*   Better approach is to use queue.
    *   at each step, count number of nodes in the level_list
        *   for each node, pop it from the list, check left and 
            right and add the value of the node to another list
            which will be appended at the end to the answer.
##  Maximum depth of binary tree:
*   I used a list to track the depth and update that
    *   My approach had bunch of redundancy.
*   simple recursion:

```python
def maxDepth(root) -> int:
    if not root:
        return 0
    return 1 + max(maxDepth(root.left), maxDepth(root.right))
``` 
## construct binary tree from pre-order and in-order:
*   Nice problem, beautiful solution
*   Divide the inorder list based on the top node
    *   Add only root at each recursion
    
```python
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.right = None
        self.left = None
        

def buildTree(preorder: [], inorder: []) -> TreeNode:
    if inorder:
        idx = inorder.index(preorder.pop(0))
        tree = TreeNode(preorder[idx])
        tree.left = buildTree(preorder, inorder[0:idx])
        tree.right = buildTree(preorder, inorder[idx+1:])
        return tree
```

## best time to buy and sell stock
*   The question is simple but the approach was kind of new to me
    *   I implemented sliding window approach o(n^2)
*   Better approach is o(n) 
    *   check value at each index
    *   if it is less than min value update it
    *   If the current value - min_val is greater than max value
        , update max value.
    
## Binary tree maximum path sum
*   Couldn't solve it, though I got the idea but couldn't 
    generalized it to other cases
*   What is happening?
    *   track the max value from left or right
    *   update max_value based on sum of left, right and curr node
        val.
    *   pass the maximum of left and right to the top stack
*   Note from solution:
    *   A path from start to end, goes up on the tree from 0
        or more steps, then goes down for 0 or more steps.
        Once it goes down, it can't go up.
        Each path has a highest node, which is also
        the lowest common ancestor of all other nodes on the path.
        
```python
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
    def maxPathSum(self, treeNode: TreeNode) -> int:
        max_value = [float('-inf')]
        self.maxPath(self, treeNode, max_value)
        return max_value[0]
    
    def maxPath(self, root: TreeNode, max_val: []) -> int:
        if not root:
            return 0
        else:
            left_val = max(0, self.maxPath(root.left, max_val))
            right_val = max(0, self.maxPath(root.right, max_val))
            max_value[0] = max(max_val[0], left_val + right_val 
                                + root.val)
            return max(left_val, right_val) + root.val
```

## Notes on graph:

*   Three ways to represent:
    *   Edge list
        *   Edges: (A, B), (A, C), (A, D), (C, D)
    *   Adjacency Matrix:
    
        |   	| A 	| B 	| C 	| D 	|
        |---	|---	|---	|---	|---	|
        | A 	| 0 	| 1 	| 1 	| 1 	|
    
    *   Adjacency List:
    
    ```python
    graph = {'A': [B, C, D]}
    ```
    
![Graph DataStructure](./images/graph_datastructure.png)

*   Graph algorithm runtime depends on |v|(#nodes/vertex), 
    and |E|(#edges)
    *   Can be o(|v| + |e|), o(|v||e|),o(|v|log|v| + |e|)
*   The density of the graph also distinguish o(|v|^(3/2)) from
    o(|e|)
*   Dense Graph:
    *   |E| &#x2248; |V^2| 
*   Sparse Graphs:
    *   |E| &#x2248; |V|
*   Lowest Common Ancestors(LCA):
    *   The lowest common ancestor between two nodes _v_ and _w_
        in a tree or Directed Acyclic Graph(DAG) of __T__ is the
        lowest (i.e deepest) node that has both _v_ and _w_ as
        descendants. Where we define each node to be a 
        descendent of itself, so if _v_ has a direct connection
        from _w_, _w_ is the lowest common ancestor.
*   Use cases of lowest common ancestors:
    *   can be used for determining the distance between pair of
        nodes:
        *   Calculate distance from _w_ to the root
        *   Calculate distance from _v_ to the root:
        *   subtract (distance from root to LCA) * 2
*   In tree data structure where each node points to parent, LCA can be found
    by finding the first intersection of the pathes from _v_ and _w_ to the root.
    *   o(h) = height of tree

*   __Directed Acyclic Graph (DAG)__ is a finite directed graph with no directed cycles.
That is, it consist of finitely many vertices and edges with each edge directed from 
one vertex to another, such that there is no way to start at any vertex and follow a consistently-directed sequence of edges that loops back to same vertex.

*   __Topological ordering__ ([Here](https://en.wikipedia.org/wiki/Topological_sorting)) is possible if and only if the graph is a DAG.
It is a sequence of the vertices (tasks) such that every edge is directed from earlier to later in sequence. For every directed edge uv from vertex u to vertex v, u comes before v in the ordering. DAG has at least one topological ordering.
The canonical application of topological sorting is in scheduling a sequence of jobs or tasks 
based on their dependencies.

*   in-degree is the number of incoming edges
*   Three algorithms are available for topological sorting
    *   DFS based approach
        *   We need stack, unlike normal DFS, the stack tracks of the last node
            in the connected edge. We insert the node at the begging of the stuck
    *   Kahn's algorithm
        *   create graph
        *   create in-degree
        *   Create list as output
        *   create queue
        *   add nodes with in-degree of zero into the queue
        *   add node with zero in-degree to the tail of the list
        *   while queue is not empty:
            *   pop node 
            *    add node with zero in-degree to the tail of the list
            *   for each neighbor of node
                *   decrease in-degree by one
                *   if neighbor has zero in-degree insert neighbor to queue
        
    *   Parallel algorithm

## valid palindrome:
*   remove characters and space
*   check the value with reverse

```python
def validPalindrom(s: str) -> bool:
    s = ''.join(e for e in s if e.isalnum()).lower()
    return s==s[::-1]
```

## Longest Consecutive Sequence
*   Coding note:
    *   pass vs continue:

```python
while this < that:
    if this <= 0:
        if this == -1:
            # do something
        else:
            # pass will run this += 1
            pass 
            # continue will not run this += 1
            continue
    this += 1
```

## absolute sorting:
*   sorting tuple:
    *   sorted(k, key=lambda x : abs(x))
*   Vowels: aeiouy
*   Consonant: letters that are not vowels

```python
# method to round the float value
round(float, num_decimal)
```

## Clone Graph:
*   Didn't know how pointers work in python
*   You can pass object as key to a dict!!
    *   Good method to use as a pointer to object 
    *   the dict object has a key value of node object and 
        value of the copy of the node.
*   We need a queue for bfs to pop the left and check all values
*   We don't need a queue for DFS 
* Note on using object as key in python dictionary:
    *   A dictionary's keys are almost arbitrary values. Values that are __not__
        Hashable, that is, values, containing lists, dictionaries or other
        mutable types (that are compared by value rather than by object
        identity) may not be used as keys.
    *   __Hashable__ :
        *   An object is hashable if it has a hash value which never changes
            during its lifetime(it needs a \_\_hash\_\_() method), and can be 
            compared to other objects (it needs an \_\_eq\_\_() or \_\_cmp\_\_()
            method). Hashable objects which compare equal must have the same 
            hash value.
        *   Hashability makes on object usable as a dictionary key and a set 
            member, because these data structure use the hash value internally.
             
```python
import collections
class Node(object):
    def __init__(self, val, neighbors: []):
        self.val = val
        self.neighbors = neighbors
        
"BFS"
def cloneGraph(node: Node) -> Node :
    top_node = Node(node.val, [])
    dic = {node: top_node}
    #dequeue item needs to be iterable 
    queue = collections.deque([node])
    while queue:
        curr_node = queue.popleft()
        for neighbor in curr_node.neighbors:
            if neighbor not in dic:
                copy_node = Node(node.val, [])
                dic[neighbor] = copy_node
                queue.append(neighbor)
                dic[curr_node].neighbors.append(copy_node)
            else:
                dic[curr_node].neighbors.append(dic[neighbor])
    return top_node
    

```

## word break:
*   dp
*   I've done the recursive approach, which at each step it will updates the 
    letter and loot for all words.
*   Looking at this solution some one can come with dp solution.
    *   the empty array, index[0] is true, the starting case, or
        if the i - len(word) == -1, it is true
    *   at each index, check the the sequence of the word from that index
        minus the length of the word. if the end-index exist and the preview 
        value also is true, set it to true.
*   graph
*   The graph approach is nice!
    *   consider the first index of letters in wordDict as vertex and edge
        is the rest of the word.
        *   If there is a path from strat to the end then we have an answer
*   Queue to insert all vertices, set to add visited index

## Linked List Cycle:
*   Make a set of visited nodes, if it reaches it again return True
*   Better approach is two runner approach:
    *   place one runner one step ahead and make it faster, some time in the 
        future the fast runner will pass the slow one.

## Re order single linked list:
*   I solved the problem by putting all nodes to the list and change the 
    direction of the pointer by popping from last and popping from start.
*   The above approach is slow
*   better approach:
    *   Find the mid point
    *   reverse points after mid point
        *   NOTE: make sure when reversing there are no links between 
            two lists
    *   merge the two 

```python
class Node(object):
    def __init__(self, x):
        self.val = x
        self.next = None

def re_order_list(head: Node):
    # find middle
    p1 = head
    p2 = head.next
    while p2 and p2.next:
        p1 = p1.next
        p2 = p2.next.next
    # reverse the mid point
    prev = None
    curr_node = p1.next
    while curr_node:
        tmp = curr_node.next
        curr_node.next = prev
        prev = curr_node
        curr_node = tmp
    rev_node = prev
    
    ## Note here, we should set p1.next to None
    ## If we don't there will be a connection between 
    ## reverse middle list and first list
    ##  which will lead to infinite link between two nodes!
    p1.next = None
    # combine two lists:
    p1 = head
    p2 = rev_node
    while p1 and p2:
        tmp1 = p1.next
        tmp2 = p2.next
        p2.next = p1.next
        p1.next = p2
        p1 = tmp1
        p2 = tmp2
```

## House Robber:
*   DP problem
    *   the answer is either the value of the previews one or the value 
        of two house behind + plus current house

##  Number of islands:
    *   Check the unit and check for all neighborse and count them as visited
    *   when exiting the loop increase counter by one

## Reverse single linked list:
*   recursive approach:
    *    if next is null, set it to head
    *   for other, set the currentNode.next.next to currentNode
    
##  Course Schedule:
*   This is no cycle in graph problem
    *   at each step check nodes and mark it as visited
    *   for each node, check if is in the being visited node or not
    *    at the end of checking, set the visited one to is visited
    
## Trie prefix data structure
*   The node has:
    *   Link which can be a dict that holds letters
    *   is_word to set as a pointer in the dict to define the node as word
    *   It has contains_prefix, add, get, set_word
*   Make Trie from the output and look at the words in matrix to find one

## Invert tree 
*   recursive:
    *   set right to left, set left to right, return root

##  Delete node:
*   find the correct replacement, get the value update the node
    *   recursively update the value from selected node until it reaches to 
        leaf
        
##  House Robber II:
*   House are circularly connected:
    *    start from the first assuming it is not robbed and go to the length - 2
    *   start from the second assuming it is not robbed and go to the length - 1
    *   Return the max between the two
    
## Least Recently Used (LRU):
*   The LRU caching scheme i to remove the least recently used frame
    when the cache is full and a new page is referenced which is 
    not there in cache.

## Kth smallest element in BST:
*   Use in order traverse to find the nth element

## Lowest Common Ancestor of a binary search tree:
*   What I've done
    *   Make search then add element to list
    *   Find the highest intersection between them

```python
list_a = [1, 2, 3]
list_b = [2, 3, 4]
values = [x for x in list_a if x in list_b]
``` 

## Product of Array except itself:
*   Didn't solve it by myself
*   The idea is to make two arrays 
    *   One holding the value of product of left of the index
    *   The other one holds the product of the right of the index
    *   the result is the product of two arrays


## Serialize and deserialize binary tree

## Longest Increasing Subsequence

## Coin Change



# Leet code Prime:

##  Meeting room:
>   Problem:
>   Given an array of meeting time intervals consisting of start and end times [[s1, e1], [s2, e2], ...] determine if a person could attend all meetings.
*   sort by first value
*   sort by second value
*   The first values should be greater than second values

Some notes about list and ndarray:
*   for getting value:

```python
input_list = [[1, 2], [3, 4], [5, 6]]

# getting with list
first_value = [x for x in (value[0] for value in input_list)]
# using map
first_values = map(lambda x: x[0], input_list)
first_values = list(first_values)

# getting first value with numpy
np_array = np.array(input_list)
first_value = np_array[:, 0]
```


## Meeting room II:
>   Problem:
>   Given an array of meeting time intervals consisting of start and end times [[s1, e1], [s2, e2], ...], find the minimum number of conference rooms required.
*   Greedy approach
    *   Sort by first value
    *   Make heap to get the minimum from the second
        value
    *   merge the next with the min value and return
        the length of the heap 
        
## Graph Valid Tree:
> Problem:
>   Given n nodes labeled from 0 to n-1 and a list of undirected edges (each edge is a pair of nodes), write a function to check whether these edges make up a valid tree

Example:

```python
# inputs:
n = 5 # edges
edges = [[0, 1], [0, 2], [0, 3], [1, 4]]
output = True

n = 5
edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]]
output = False
```

What is unidirected graph?

An unidirected graph is graph that are connected together, where all the edges are bidirectional.

A unidirected graph is tree if it has following properties:

*   There is no cycle
*   The graph is connected

Very good resource:
https://mathinsight.org/definition/undirected_graph

What is valid tree?
No shared child is allowed, parents have unique child

Here are some approaches to solve this problem:

__BFS Solution__:

we need to know the connection of one node to it's neighbors.
We make a hash map of nodes as key and neighbors as a list of nodes.

__NOTE__: To make sure while traversing, each node and their neighbors are visited and because it is bidirectional, during making this data structure, we need to create one for first node and the other of the neighbor node.

For BFS, we need queue, we pop the first node, look at the neighbors, we add the 
neighbors to the queue to get visited at each loop.

__NOTE__: because of unidirectional, when visiting the node we need to mark it
as visited. For example if we have [[0,1],[0,2],[0,3],[1,4]], bidirectional connection
make the node that already visited to appears again which will give wrong result.

Run Time complexity:

We dequeue from the our queue the top node, there will be V visited to each node, 
Each vertex is enqueud and dequeued at most once.
Scanning for all adjacent vertices takes o(|E|) time, since sum of lengths of adjacency lists is |E| So the time complexity will be O(|V| + |E|), this is true for general BFS.

The difference is here, at each neighbor we need to check at all queue if it is visited or not, which makes is |VE|. so the time complexity is o(V + VE)

even better way to understand:

```python
# time complexity V
while graph is not empty:
    #o(1)
    # get vertex
    # O(edj) : where edj is number of adjacent edges for current vertex
        # add neighbors
    #O(1)
    # remove current vertex
```

So we will have:

V * (O(1) + O(edj) + O(1))

V + V * edj + V

2V + E(Total number of edges in graph)

V + E

Making the node structure is O(V)

__DFS solution__:

The approach is to find the cycle using DFS.
We use a set to store the visited one, and exclude the parent node during recursion
Because of the unidirectional approach

__Union solution__:

The union solution is part of [disjoint set data structure](https://en.wikipedia.org/wiki/Disjoint-set_data_structure).

[link](https://www.geeksforgeeks.org/union-find/) to geek for geeks.

The every node by itself is a subset of itself, which means the parent to the node is itself.
So we initialize the sets as -1 as a subset of itself.
During each visit of the edges, we find the parent node of both vertices.
If both values are -1 then we make a new subset, and set one node to another node using an array.
If there is a cycle in the graph then the node will have same parent node in the subset which is not -1.
The time complexity of this approach is O(E), because we only need to check the edges to find the parent of each vertex and unify them.

## Alien Dictionary:

> Problem
>
>   There is a new alien language which uses the latin alphabet. However, the order among letters
>   are unknown to you. You receive a list of non-empty words from the dictionary, where words are sorted lexicographically by the rules of this new language. Derive the order of letters in this language.

Example:

```python
input = ["wrt", "wrf", "er", "ett", "rftt"]
output = "wertf"

Input = ["z", "x"]
output = "zx"

input = ["z", "x", "z"]
output = ""
```


## Encode and Decode Strings:

## Number of Connected Components in an Unidirected graph:

