# Created by dark at 10/23/2019, 1:50 PM
def frequency_sort(items):
    counter = collections.Counter(items)
    keys = [k for k in counter.keys()]
    values = [v for v in counter.values()]
    ans = []
    while keys:
        max_value = max(values)
        idx = values.index(max_value)
        key = keys[idx]
        for i in range(max_value):
            ans.append(key)
        values.pop(idx)
        keys.pop(idx)
    return ans